/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.model;

import java.util.List;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.common.AttrSetCriteria;
import org.butor.attrset.common.AttrSetServices;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.springframework.transaction.annotation.Transactional;

public class AttrSetModel implements AttrSetServices {
	private AttrSetDao dao;
	
	
	@Override
	public void getAttrSet(final Context ctx, final AttrSetCriteria criteria) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		List<AttrSet> list = dao.getAttrSet(criteria, cra);
		rh.addRow(list);
	}
	
	@Override
	@Transactional
	public void updateAttrSet(final Context ctx, final List<AttrSet> attrSet) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		for (AttrSet a : attrSet) {
			AttrSet updated = dao.updateAttrSet(a, cra);
			rh.addRow(updated);
		}
	}
	
	@Override
	@Transactional
	public void insertAttrSet(final Context ctx, final List<AttrSet> attrSet) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<Object> rh = ctx.getResponseHandler();
		for (AttrSet a : attrSet) {
			AttrSet inserted = dao.insertAttrSet(a, cra);
			rh.addRow(inserted);
		}
	}
	
	@Override
	@Transactional
	public void deleteAttrSet(final Context ctx, final List<AttrSet> attrSet) {
		CommonRequestArgs cra = ctx.getRequest();
		for (AttrSet a : attrSet) {
			dao.deleteAttrSet(AttrSetCriteria.valueOf(a.getType(),a.getId(),a.getK1(), a.getK2()), cra);
		}
	}
	public AttrSetDao getDao() {
		return dao;
	}
	public void setDao(AttrSetDao dao) {
		this.dao = dao;
	}
}
