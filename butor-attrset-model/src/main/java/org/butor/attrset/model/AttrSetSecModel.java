/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.model;

import java.util.List;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.common.AttrSetCriteria;
import org.butor.attrset.common.AttrSetSecServices;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.AuthModel;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.springframework.transaction.annotation.Transactional;

public class AttrSetSecModel implements AttrSetSecServices {
	private AttrSetDao dao;
	private AuthModel authModel;
	private final String SYSTEM = "sec";
	private final String FUNCTION = "attrSet";	

	@Override
	public void getAttrSet(final Context<List<AttrSet>> ctx, final AttrSetCriteria criteria) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<List<AttrSet>> rh = ctx.getResponseHandler();
		if (authModel.hasAccess(SYSTEM, FUNCTION, AccessMode.READ, cra)) {
			List<AttrSet> list = dao.getAttrSet(criteria, cra);
			rh.addRow(list);
		}else{
			ApplicationException.exception(AuthMessageID.ACCESS_DENIED.getMessage());
			
		}
	}

	@Override
	@Transactional
	public void insertAttrSet(final Context<AttrSet> ctx, final AttrSet attrSet) {
		CommonRequestArgs cra = ctx.getRequest();
		ResponseHandler<AttrSet> rh = ctx.getResponseHandler();
		if (authModel.hasAccess(SYSTEM, FUNCTION, AccessMode.WRITE, cra)) {
			AttrSet inserted = dao.insertAttrSet(attrSet, cra);
			rh.addRow(inserted);
		}else{
			ApplicationException.exception(AuthMessageID.ACCESS_DENIED.getMessage());
		}
	}

	@Override
	@Transactional
	public void deleteAttrSet(final Context<AttrSet> ctx, final AttrSet attrSet) {
		CommonRequestArgs cra = ctx.getRequest();
		if (authModel.hasAccess(SYSTEM, FUNCTION, AccessMode.WRITE, cra)) {
			dao.deleteAttrSet(
					AttrSetCriteria.valueOf(attrSet.getType(), attrSet.getId(), attrSet.getK1(), attrSet.getK2()), cra);
		}else{
			ApplicationException.exception(AuthMessageID.ACCESS_DENIED.getMessage());
		}
	}

	@Override
	@Transactional
	public void updateAttrSet(final Context<AttrSet> ctx, final AttrSet attrSet) {
		CommonRequestArgs cra = ctx.getRequest();
		if (authModel.hasAccess(SYSTEM, FUNCTION, AccessMode.WRITE, cra)) {
			/*TODO get the response handler and return what is return by the method updateAttrSet*/
			dao.updateAttrSet(attrSet, cra);
		}else{
			ApplicationException.exception(AuthMessageID.ACCESS_DENIED.getMessage());
		}
	}

	public AttrSetDao getDao() {
		return dao;
	}

	public void setDao(AttrSetDao dao) {
		this.dao = dao;
	}

	public AuthModel getAuthModel() {
		return authModel;
	}

	public void setAuthModel(AuthModel authModel) {
		this.authModel = authModel;
	}

}
