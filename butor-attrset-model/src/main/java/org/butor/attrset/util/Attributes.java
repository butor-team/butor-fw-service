/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.util;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.json.CommonRequestArgs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;

/**
 * Attributes is an helper class to work with AttrSet that are stored in the database
 * This class is not thread-safe.
 * 
 * @author tbussier
 *
 */
public interface Attributes {

	/**
	 * Return a list of the COPY of the attributes ORDERED by seq , so the caller this method
	 * that change the internal states of the attributes will have no effect on the attributes 
	 * stored in this class. 
	 * 
	 * This has been done to insure the proper behavior of the Attributes object.
	 *   
	 * @return
	 */
	public abstract Collection<AttrSet> list();

	public abstract String get(String k1);

	public abstract String get(String k1, String k2);

	public abstract AttrSet getAttr(String k1);

	public abstract AttrSet getAttr(String k1, String k2);

	/**
	 * Set the value for k1-k2 at sequence 0.
	 * 
	 * 
	 * @param k1
	 * @param k2
	 * @param value
	 * @return
	 */
	public abstract boolean set(String k1, String k2, String value);

	public abstract boolean set(String k1, int seq, String value);

	public abstract boolean set(String k1, String value);

	public abstract boolean set(String k1, String k2, int seq, String value);

	public abstract boolean delete(String k1, String k2);

	/**
	 * The method saves the state of the Attribute Set 
	 * this object is working with.
	 * 
	 * It is recommended to execute this method within a transaction.
	 */
	public abstract void save();
	

	/**
	 * Lock the attribute set make others wait and block on
	 * reading it. Useful to synchronise execution of a piece code in
	 * distributed environment.
	 * 
	 * @throws UnsupportedOperationException if the implementation do
	 * not support locking.
	 */
	public abstract void lock() throws UnsupportedOperationException;
	
	
	public static class AttributesBuilder {
		private String id;
		private String type;
		private String k1;
		private AttrSetDao dao;
		private CommonRequestArgs cra;
		private CacheBuilder<Object, Object> cacheBuilder;
		final private Logger logger = LoggerFactory.getLogger(getClass());

		public Attributes build() {
			//TODO preconditions
			Preconditions.checkNotNull(dao,"Dao must not be null!");
			Preconditions.checkNotNull(type,"Type must not be null!");
			Preconditions.checkNotNull(id,"Id must not be null!");
			Preconditions.checkNotNull(cra,"CommonRequestArgs must not be null!");
			
			if (cacheBuilder != null) {
				LoadingCache<CachedAttributes.K, Attributes> attributeCache = cacheBuilder.removalListener(new RemovalListener<CachedAttributes.K, Attributes>() {
						@Override
						public void onRemoval(RemovalNotification<CachedAttributes.K, Attributes> notification) {
							logger.info("Removing attributes type:{} id:{} from cache. Cause:{}",new Object[]{type,id,notification.getCause()});
						}
						
						}).build(new CacheLoader<CachedAttributes.K , Attributes>(){
								@Override
								public Attributes load(CachedAttributes.K key) throws Exception {
									return new AttributesFromDao(dao,type,id,k1,cra);
								}
								
						});
				return new CachedAttributes(attributeCache);
			} else {
				return new AttributesFromDao(dao,type,id,k1,cra);
			}
		}

		
		public AttributesBuilder setId(String id) {
			this.id = id;
			return this;
		}
		
		public AttributesBuilder setK1(String k1) {
			this.k1 = k1;
			return this;
		}
		
		public AttributesBuilder refreshAfter(long duration, TimeUnit unit) {
			cacheBuilder  = CacheBuilder.newBuilder().expireAfterWrite(duration, unit);
			return this;
		}

		public AttributesBuilder setType(String type) {
			this.type = type;
			return this;
		}
		
		public AttributesBuilder setAttrSetDao(AttrSetDao dao) {
			this.dao = dao;
			return this;
		}

		public AttributesBuilder setCommonRequestArgs(CommonRequestArgs cra) {
			this.cra = cra;
			return this;
		}
		
	}
	
	
}
