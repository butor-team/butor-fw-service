/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.common.AttrSetCriteria;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.json.CommonRequestArgs;
import org.springframework.beans.BeanUtils;

class AttributesFromDao implements Attributes {
	final private AttrSetDao dao;
	final private String id;
	final private String type;
	final private String k1;
	final private CommonRequestArgs cra;
	private int reqIdSeq=0; 
	
	private boolean changePersisted=true;
	private final Map<AttributeKey, AttrSetWithAction> attributeMap = new HashMap<AttributeKey, AttrSetWithAction>();
	

	AttributesFromDao(AttrSetDao dao, String type, String id, String k1, CommonRequestArgs cra) {
		super();
		this.dao = dao;
		this.id = id;
		this.type = type;
		this.k1 = k1;
		this.cra = cra;
		if (cra.getReqId() == null) {
			cra.setReqId("R-"+UUID.randomUUID().toString());
		}
		refreshAttributeMap();
	}
	AttributesFromDao(AttrSetDao dao, String type, String id, CommonRequestArgs cra) {
		this(dao, type, id, null, cra);
	}


	private void refreshAttributeMap() {
		attributeMap.clear();
		List<AttrSet> listFromDB = this.dao.getAttrSet(AttrSetCriteria.valueOf(this.type, this.id, this.k1, null), createCommonRequestArgs());
		for (AttrSet a : listFromDB) {
			AttributeKey ak = new AttributeKey(a.getK1(), a.getK2());
			attributeMap.put(ak, new AttrSetWithAction(a));
		}
	}
	
	/**
	 * Lock the attribute set make others wait and block on
	 * reading it. Useful to synchronise execution of a piece code in
	 * distributed environment.
	 * 
	 * @throws UnsupportedOperationException if the implementation do
	 * not support locking.
	 */
	public void lock() throws UnsupportedOperationException {
		attributeMap.clear();
		List<AttrSet> listFromDB = this.dao.getAndLockAttrSet(AttrSetCriteria.valueOf(this.type, this.id, this.k1, null), createCommonRequestArgs());
		for (AttrSet a : listFromDB) {
			AttributeKey ak = new AttributeKey(a.getK1(), a.getK2());
			attributeMap.put(ak, new AttrSetWithAction(a));
		}
	}
	
	
	private CommonRequestArgs createCommonRequestArgs() {
		CommonRequestArgs newReq = new CommonRequestArgs();
		BeanUtils.copyProperties(cra,newReq);
		String oriRequestId = cra.getReqId();
		newReq.setReqId(oriRequestId+"-"+reqIdSeq++);
		return newReq;
	}
	
	/* (non-Javadoc)
	 * @see org.butor.attrset.util.Titi#list()
	 */
	@Override
	public Collection<AttrSet> list() {
		List<AttrSet> list = new ArrayList<AttrSet>();
		for (AttrSetWithAction as : attributeMap.values()) {
			AttrSet newAs = new AttrSet();
			BeanUtils.copyProperties(as.attrSet, newAs);
			list.add(newAs);
		}
		Collections.sort(list,new Comparator<AttrSet>() {
			@Override
			public int compare(AttrSet o1, AttrSet o2) {
				return o1.getSeq() - o2.getSeq();
			}
		});
		return list;
	}

	@Override
	public String get(String k1) {
		return get(k1,null);
	}
	@Override
	public String get(String k1, String k2) {
		AttributeKey ak = new AttributeKey(k1,k2);
		AttrSetWithAction as = attributeMap.get(ak);
		if (as != null && as.action != Action.DELETE) {
			return as.attrSet.getValue();
		}
		return null;
	}

	@Override
	public AttrSet getAttr(String k1) {
		return getAttr(k1,null);
	}
	@Override
	public AttrSet getAttr(String k1, String k2) {
		AttributeKey ak = new AttributeKey(k1,k2);
		AttrSetWithAction as = attributeMap.get(ak);
		if (as != null && as.action != Action.DELETE) {
			AttrSet asc = new AttrSet();
			BeanUtils.copyProperties(as.attrSet, asc);
			return asc;
		}
		return null;
	}
	@Override
	public boolean set(String k1, String k2, String value) {
		return set(k1,k2,0,value);
	}

	/* (non-Javadoc)
	 * @see org.butor.attrset.util.Titi#set(java.lang.String, int, java.lang.String)
	 */
	@Override
	public boolean set(String k1,int seq, String value) {
		return set(k1,null,seq,value);
	}

	/* (non-Javadoc)
	 * @see org.butor.attrset.util.Titi#set(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean set(String k1, String value) {
		return set(k1,0,value);
	}

	
	/* (non-Javadoc)
	 * @see org.butor.attrset.util.Titi#set(java.lang.String, java.lang.String, int, java.lang.String)
	 */
	@Override
	public boolean set(String k1, String k2, int seq, String value) {
		AttributeKey ak = new AttributeKey(k1,k2);
		AttrSetWithAction as = attributeMap.get(ak);
		// new attribute 
		if (as == null)  {
			AttrSet newAs = new AttrSet();
			newAs.setType(type);
			newAs.setId(id);
			newAs.setK1(k1);
			newAs.setK2(k2);
			newAs.setSeq(seq);
			newAs.setValue(value);
			AttrSetWithAction insertAs = new AttrSetWithAction(newAs);
			insertAs.action=Action.ADD;
			attributeMap.put(ak, insertAs);
			changePersisted=false;
			return true;
		} else {
			//update attribute if value changed
			if ((value != null && !value.equals(as.attrSet.getValue())) || (value == null && as.attrSet.getValue() != null )) {
				if (as.action == Action.DELETE) {
					// already flagged for deletion, make up your mind!
					return false;
				}
				if (as.action == Action.NONE) {
					as.action = Action.MODIFY;
				} 
				as.attrSet.setValue(value);
				changePersisted=false;
				return true;
			}
		}
		return false;
		
	}
	
	/* (non-Javadoc)
	 * @see org.butor.attrset.util.Titi#delete(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean delete(String k1, String k2) {
		AttributeKey ak = new AttributeKey(k1,k2);
		AttrSetWithAction as =  attributeMap.get(ak);
		if (as != null)  {
			as.action = Action.DELETE;
			attributeMap.put(ak, as);
			changePersisted=false;
			return true;
		} 
		return false;
	}

	/* (non-Javadoc)
	 * @see org.butor.attrset.util.Titi#save()
	 */
	@Override
	public void save() {
		if (!changePersisted) {
			for (Entry<AttributeKey, AttrSetWithAction> entry  : attributeMap.entrySet()) {
				CommonRequestArgs request = createCommonRequestArgs(); 
				AttributeKey ak = entry.getKey();
				AttrSetWithAction attrSetAction = entry.getValue();
				AttrSet as  = attrSetAction.attrSet;
				switch (attrSetAction.action) {
				case ADD:
					dao.insertAttrSet(as, request);
					break;
				case MODIFY:
					dao.updateAttrSet(as, request);
					break;
				case DELETE:
					dao.deleteAttrSet(AttrSetCriteria.valueOf(type, id, ak.k1,ak.k2), request);
					break;
				case NONE:
					break;
					
				}
			}
			refreshAttributeMap();
			changePersisted=true;
		}
		
	}
	
	
	private static enum Action {
		NONE,
		DELETE,
		ADD,
		MODIFY;
	}
	
	private static class AttrSetWithAction   {
		private final AttrSet attrSet;
		private Action action = Action.NONE; 

		private AttrSetWithAction(AttrSet attrSet) {
			super();
			this.attrSet = attrSet;
		}

		@Override
		public String toString() {
			return "AttrSetWithAction [attrSet=" + attrSet + ", action=" + action + "]";
		}
		
	}
	private static class AttributeKey {
		private AttributeKey(String k1, String k2) {
			super();
			this.k1 = k1 == null ? "" : k1;
			this.k2 = k2 == null ? "" : k2;
		}
		private final String k1;
		private final String k2;
		

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((k1 == null) ? 0 : k1.hashCode());
			result = prime * result + ((k2 == null) ? 0 : k2.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AttributeKey other = (AttributeKey) obj;
			if (k1 == null) {
				if (other.k1 != null)
					return false;
			} else if (!k1.equals(other.k1))
				return false;
			if (k2 == null) {
				if (other.k2 != null)
					return false;
			} else if (!k2.equals(other.k2))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "KEY[k1=" + k1 + ", k2=" + k2 +"]";
		}
		
		
	}


	@Override
	public String toString() {
		return "Attributes [changePersisted=" + changePersisted + ", attributeMap=" + attributeMap + "]";
	}
}
