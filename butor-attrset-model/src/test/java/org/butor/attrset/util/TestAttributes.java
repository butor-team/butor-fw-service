/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.attrset.common.AttrSet;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.attrset.util.Attributes.AttributesBuilder;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.ResponseHandler;
import org.butor.test.ButorTestUtil;
import org.butor.utils.ApplicationException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;


//@RunWith(value=SpringJUnit4ClassRunner.class)
//@ContextConfiguration(value="classpath:test-context.xml")
public class TestAttributes {

	ButorTestUtil testUtil = new ButorTestUtil();

	@SuppressWarnings("unchecked")
	ResponseHandler<Object> responseHandler = mock(ResponseHandler.class);
	
	
	@Resource
	DataSource dataSource;
	@Resource(name="attrSetDaoTest")
	AttrSetDao attrSetDao;
	
	CommonRequestArgs cra = ButorTestUtil.getCommonRequestArgs("{userId=mrbig, sessionId=session}");

	@BeforeClass
	public static void bootstrapUnitTest() throws Exception {
		BasicConfigurator.configure();
	}
	
	@Before 
	public void init() throws Exception{
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		testUtil.executeSQL(jt, ResourceUtils.getURL("classpath:attrset-db.sql"),"");
		testUtil.executeSQL(jt, ResourceUtils.getURL("classpath:init-test.sql"),"");
	}


	//@Test
	public void testSimple() {
		AttributesBuilder ab = new AttributesBuilder().setCommonRequestArgs(cra).setAttrSetDao(attrSetDao).setId("test")
				.setType("codeset");
		Attributes attr = ab.build();
		assertNotNull(attr);
	}

	//@Test
	public void testList() throws Exception {
		AttributesBuilder ab = new AttributesBuilder().setCommonRequestArgs(cra).setAttrSetDao(attrSetDao).setId("test")
				.setType("codeset");
		AttributesBuilder cab = new AttributesBuilder().setCommonRequestArgs(cra).setAttrSetDao(attrSetDao).setId("test")
				.setType("codeset").refreshAfter(1, TimeUnit.MINUTES);
	

		for (Attributes attr : new Attributes [] { ab.build(), cab.build(), }) {
			init();
			
			assertNotNull(attr);
			Collection<AttrSet> list = attr.list();
			assertEquals(4,list.size());
			AttrSet as = list.iterator().next();
			list.remove(as);
			assertEquals(3,list.size());
			//make sure the list is recreated from scratch
			Collection<AttrSet> list2 = attr.list();
			assertEquals(4,list2.size());
			
			//make sure the sequence order is respected.
			int i=1;
			for (AttrSet next : list2) {
				assertEquals(i,next.getSeq());
				i++;
			}
			
			//Test UPDATE
			
			String vk2 = attr.get("k2", null);
			assertEquals("key2",vk2);
			attr.set("k2", null, "key2-modified");
			
			vk2 = attr.get("k2", null);
			assertEquals("key2-modified",vk2);
			
			
			
			attr.save();
	
			// should be still there in attr
			vk2 = attr.get("k2", null);
			assertEquals("key2-modified",vk2);
	
			
	
			attr.delete("k2", null);
			assertNull(attr.get("k2",null));
			attr.save();
	
			list = attr.list();
			assertEquals(3,list.size());
			
	
			attr.save();
		}
		
	}
	
	//@Test
	public void testConcurrentUpdate() {
		AttributesBuilder ab = new AttributesBuilder().setCommonRequestArgs(cra).setAttrSetDao(attrSetDao).setId("test")
				.setType("codeset");
		Attributes attr = ab.build();
		Attributes attr2 = ab.build();
		
		String vk2 = attr.get("k2", null);
		assertEquals("key2",vk2);
		
		vk2 = attr2.get("k2", null);
		assertEquals("key2",vk2);
		
		attr2.set("k2", null, "myvalAttr2");
		attr.set("k2", null, "myvalAttr");
		
		attr.save();
		try {
			attr2.save();
			fail("Should fail!");
		} catch (ApplicationException e) {
			assertEquals(DAOMessageID.UPDATE_FAILURE.getMessage(), e.getMessages()[0]);
		}
	}

}
