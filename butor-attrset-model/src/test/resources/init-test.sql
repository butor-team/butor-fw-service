--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

INSERT INTO attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
('codeset','test','k1', 1, '', 'key1', CURRENT_TIMESTAMP, 'load',0),
('codeset','test','k2', 2, '', 'key2',CURRENT_TIMESTAMP, 'load',0),
('codeset','test','k3', 4, 'k31', 'key31', CURRENT_TIMESTAMP, 'load',0),
('codeset','test','k3', 3, 'k32', 'key32', CURRENT_TIMESTAMP, 'load',0);

