/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.mule.common.ServiceCall;
import org.butor.utils.ApplicationException;

public class ServiceCallDaoImpl extends AbstractDao implements ServiceCallDao {

	private String insertSql;
	private String listServiceCallsByUser;
	private String listServiceCallsByService;

	private final String PROC_INSERT = getClass().getName() + ".insertSql";
	private final String PROC_LIST_SERVICECALLS_BY_USER = getClass().getName()
			+ ".listServiceCallsByUser";
	private final String PROC_LIST_SERVICECALLS_BY_SERVICE = getClass()
			.getName() + ".listServiceCallsByService";

	@Override
	public void insertServiceCall(ServiceCall criteria, CommonRequestArgs cra) {
		criteria.setStamp(new Date());
		criteria.setRevNo(0);
		if (criteria.getPayload().length() > 21000) {
			criteria.setPayload(criteria.getPayload().substring(0, 21000));
		}
		UpdateResult ur = insert(PROC_INSERT, insertSql, criteria, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.INSERT_FAILURE
					.getMessage());
		}
	}

	@Override
	public List<ServiceCall> listServiceCalls(
			CommonRequestArgs commonRequestArgs) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ServiceCall> listServiceCallsByUser(String user,
			CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("user", user);
		return queryList(PROC_LIST_SERVICECALLS_BY_USER,
				this.listServiceCallsByUser, ServiceCall.class, args, cra);
	}

	@Override
	public List<ServiceCall> listServiceCallsByService(String service,
			CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("service", service);
		return queryList(PROC_LIST_SERVICECALLS_BY_SERVICE,
				this.listServiceCallsByService, ServiceCall.class, args, cra);
	}

	public void setInsertSql(String insertSql) {
		this.insertSql = insertSql;
	}

	public void setListServiceCallsByUser(String listServiceCallsByUser) {
		this.listServiceCallsByUser = listServiceCallsByUser;
	}

	public void setListServiceCallsByService(String listServiceCallsByService) {
		this.listServiceCallsByService = listServiceCallsByService;
	}
}
