--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

DROP TABLE IF EXISTS serviceCall;
CREATE TABLE `serviceCall` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` varchar(128) NOT NULL DEFAULT '',
  `namespace` varchar(128) NOT NULL DEFAULT '',
  `service` varchar(128) NOT NULL DEFAULT '',
  `payload` varchar(21000) NOT NULL DEFAULT '',
  `success` boolean NOT NULL DEFAULT false,
  `duration` bigint(20) NOT NULL DEFAULT '0',
  `serviceCallTimestamp` datetime NOT NULL,
  `reqId` varchar(128) NOT NULL DEFAULT '',
  `sessionId` varchar(128) NOT NULL DEFAULT '',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `revNo` int(11) NOT NULL DEFAULT '0',
  `userId` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `idx_serviceCallTimestamp` (`serviceCallTimestamp`),
  KEY `idx_user` (`user`),
  KEY `idx_service` (`service`),
  KEY `idx_session` (`sessionId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;