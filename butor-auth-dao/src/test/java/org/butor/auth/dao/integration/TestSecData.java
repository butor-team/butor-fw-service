/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.auth.SecData;
import org.butor.auth.dao.DataDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.butor.utils.ApplicationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;

@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecData {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="jdbcDriverType")
	String jdbcDriverType;

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource
	DataDao dataDao;

	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-db.sql"), "TESTDB", "mysql");
	}

	@Test
	public void testReadItem() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		SecData data = new SecData();
		data.setDataId(Long.valueOf(1));
		data.setSys("sec");
		data.setDataType("firm");
		data.setD1("*");
		data.setD2("");
		data.setD3("");
		data.setD4("");
		data.setD5("");
		
		SecData readData = dataDao.readItem(data, cra);
		
		Assert.assertNotNull(readData);
	}
	
	@Test
	public void testListData() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
	
		SecData data = new SecData();
		data.setSys("sec");
		data.setD1("*");
		
		List<SecData> listData = dataDao.listData(data, cra);
		
		Assert.assertEquals(5, listData.size());
		System.out.println("Got functions: " + listData);
	}

	@Test
	public void testDelete() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		SecData data = new SecData();
		data.setSys("sec");
		data.setD1("*");
		
		List<SecData> listData = dataDao.listData(data, cra);
		
		Assert.assertEquals(5, listData.size());

		dataDao.deleteItem(listData.get(0), cra);
		
		listData = dataDao.listData(data, cra);
		
		Assert.assertEquals(4, listData.size());
		
		dataDao.deleteData(Long.valueOf(1), cra);
		
		listData = dataDao.listData(data, cra);
		
		Assert.assertEquals(0, listData.size());
	}


	@Test
	public void testInsertAndUpdate() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		SecData data = new SecData();
		data.setDataId(Long.valueOf(2));
		data.setSys("sec");
		data.setDataType("firm");
		data.setD1("test");
		
		List<SecData> listData = dataDao.listData(data, cra);
		
		Assert.assertEquals(0, listData.size());

		data.setD2("");
		data.setD3("");
		data.setD4("");
		data.setD5("");
		
		long dataId = dataDao.insertData(data, cra);
		
		data.setD2(null);
		data.setD3(null);
		data.setD4(null);
		data.setD5(null);

		listData = dataDao.listData(data, cra);
		Assert.assertEquals(1, listData.size());
		Assert.assertEquals("", listData.get(0).getD3());
		
		listData.get(0).setD3("test");
		
		dataId = dataDao.updateData(dataId, listData, cra);
		
		listData = dataDao.listData(data, cra);
		Assert.assertEquals(1, listData.size());
		Assert.assertEquals("test", listData.get(0).getD3());
		
		
	}

	@Test
	public void testValidateData() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		SecData data = new SecData();
		data.setSys("sec");
		data.setDataType("firm");
		data.setD1("*");
		
		try {
			dataDao.validateData(data);
		} catch (ApplicationException e) {
			e.printStackTrace();
			Assert.assertTrue(false);
		}

	}
}
