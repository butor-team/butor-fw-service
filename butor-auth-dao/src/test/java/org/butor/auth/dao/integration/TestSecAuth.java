/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.AuthData;
import org.butor.auth.common.ListAuthDataCriteria;
import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.func.Func;
import org.butor.auth.dao.AuthDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.butor.utils.AccessMode;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;

@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecAuth {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="jdbcDriverType")
	String jdbcDriverType;

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource(name="secAuthDao")
	AuthDao authDao;

	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-db.sql"), "TESTDB", jdbcDriverType);
	}

	
	@Test
	public void testHasAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");

		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		Assert.assertTrue(authDao.hasAccess("sysTest", "funcTest", AccessMode.READ, cra));
		Assert.assertFalse(authDao.hasAccess("sysTest", "funcTest", AccessMode.WRITE, cra));

		
		List<Func> funcs = authDao.listAuthFunc(cra);
		assertNotNull(funcs);
		System.out.println("Got functions: " +funcs);
	}
	
	@Test
	public void testListAuthFunc() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		List<Func> funcs = authDao.listAuthFunc(cra);
		Assert.assertEquals(8, funcs.size());
		System.out.println("Got functions: " +funcs);
	}

	@Test
	public void testListAuthWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		ListAuthCriteria crit = new ListAuthCriteria();
		List<Auth> listAuth = authDao.listAuth(crit, cra);
		
		Assert.assertEquals(6, listAuth.size());
	}

	@Test
	public void testListAuthWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'testUser1'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		ListAuthCriteria crit = new ListAuthCriteria();
		List<Auth> listAuth = authDao.listAuth(crit, cra);
		
		Assert.assertEquals(1, listAuth.size());
	}


	@Test
	public void testListUserAuthFunc() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		tu.executeSQL(jt, "select * from secAuth", "TESTDB", jdbcDriverType);
		
		List<Func> listFunc = authDao.listAuthFunc(cra);
		
		Assert.assertEquals(8, listFunc.size());
	}

	
	@Test
	public void testListAuthData() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		ListAuthDataCriteria crit = new ListAuthDataCriteria();
		crit.setFunc("groups").setSys("sec").setDataTypes("group").setAccessMode(AccessMode.WRITE);
		List<AuthData> data = authDao.listAuthData(crit, cra);
		Assert.assertEquals(1, data.size());
		System.out.println("Got authorized data: " +data);
	}
	
	@Test
	public void testPrepareAuthData() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		Map<String, List<String>> authDatas = authDao.prepareAuthData("sec", "firms", AccessMode.READ, cra, "user", "func");
	
		Assert.assertEquals(2, authDatas.size());
		System.out.println("Got authorized data: " +authDatas);
	}

	
	@Test
	public void testReadAuthWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		ListAuthCriteria crit = new ListAuthCriteria();
		Auth auth = authDao.readAuth(1, cra);
		
		Assert.assertNotNull(auth);
	}

	
	@Test
	public void testReadAuthWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'testUser1'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		Auth auth = authDao.readAuth(1, cra);
		Assert.assertNull(auth);
		
		auth = authDao.readAuth(2, cra);
		Assert.assertNull(auth);
		
		auth = authDao.readAuth(3, cra);
		Assert.assertNull(auth);
		
		auth = authDao.readAuth(4, cra);
		Assert.assertNull(auth);
		
		auth = authDao.readAuth(5, cra);
		Assert.assertNull(auth);
		
		auth = authDao.readAuth(6, cra);
		Assert.assertNotNull(auth);
		
	}

	@Test
	public void testInsert() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		Auth auth = new Auth();
		auth.setDataId(Long.valueOf(1));
		auth.setMode(1);
		auth.setSys("sysTest");
		auth.setWhat("functionTest");
		auth.setWhatType("func");
		auth.setWho("unitTest");
		auth.setWhoType("user");		
		
		AuthKey authKey = authDao.insertAuth(auth, cra);
		
		Auth readAuth = authDao.readAuth(authKey.getAuthId(), cra);
		
		Assert.assertEquals(auth.getDataId(), readAuth.getDataId());
		Assert.assertEquals(auth.getMode(), readAuth.getMode());
		Assert.assertEquals(auth.getSys(), readAuth.getSys());
		Assert.assertEquals(auth.getWhat(), readAuth.getWhat());
		Assert.assertEquals(auth.getWhatType(), readAuth.getWhatType());
		Assert.assertEquals(auth.getWho(), readAuth.getWho());
		Assert.assertEquals(auth.getWhoType(), readAuth.getWhoType());
		
	}

	@Test
	public void testUpdate() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		Auth auth = authDao.readAuth(2, cra);
		Assert.assertNotNull(auth);
		
		auth.setDataId(Long.valueOf(1));
		auth.setMode(1);
		auth.setSys("sysTest");
		auth.setWhat("functionTest");
		auth.setWhatType("func");
		auth.setWho("unitTest");
		auth.setWhoType("user");		
		
		AuthKey authKey = authDao.updateAuth(auth, cra);
		
		Auth readAuth = authDao.readAuth(authKey.getAuthId(), cra);
		
		Assert.assertEquals(auth.getDataId(), readAuth.getDataId());
		Assert.assertEquals(auth.getMode(), readAuth.getMode());
		Assert.assertEquals(auth.getSys(), readAuth.getSys());
		Assert.assertEquals(auth.getWhat(), readAuth.getWhat());
		Assert.assertEquals(auth.getWhatType(), readAuth.getWhatType());
		Assert.assertEquals(auth.getWho(), readAuth.getWho());
		Assert.assertEquals(auth.getWhoType(), readAuth.getWhoType());
		

	}

	@Test
	public void testDelete() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", jdbcDriverType);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecAuth.sql"), "TESTDB", jdbcDriverType);
		
		authDao.deleteAuth(new AuthKey(4, 0), cra);
		
		Auth readAuth = authDao.readAuth(4, cra);
		Assert.assertNull(readAuth);
		
	}

	
}
