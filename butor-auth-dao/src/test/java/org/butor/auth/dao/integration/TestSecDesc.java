/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.desc.Desc;
import org.butor.auth.common.desc.DescKey;
import org.butor.auth.dao.DescDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;

@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecDesc {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="jdbcDriverType")
	String jdbcDriverType;

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource
	DescDao descDao;

	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-db.sql"), "TESTDB", "mysql");
	}

	@Test
	public void testReadDesc() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		Desc desc = descDao.readDesc(new DescKey("sec.admin", "role", 0), cra);
		
		Assert.assertNotNull(desc);
		
	}
	

	@Test
	public void testDelete() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		Desc desc = descDao.readDesc(new DescKey("sec.admin", "role", 0), cra);
		Assert.assertNotNull(desc);

		descDao.deleteDesc(new DescKey("sec.admin", "role", 0), cra);
		
		desc = descDao.readDesc(new DescKey("sec.admin", "role", 0), cra);
		Assert.assertNull(desc);
		
	}


	@Test
	public void testInsertAndUpdate() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		Desc readDesc = descDao.readDesc(new DescKey("testId", "testType", 0), cra);
		Assert.assertNull(readDesc);

		Desc desc = new Desc();
		desc.setDescription("testDescription");
		desc.setId("testId");
		desc.setIdType("testType");
		
		DescKey descKey = descDao.insertDesc(desc, cra);
		
		readDesc = descDao.readDesc(new DescKey("testId", "testType", 0), cra);
		Assert.assertNotNull(readDesc);
		Assert.assertEquals("testDescription", readDesc.getDescription());
		
		desc.setDescription("testDescription2");
		
		descKey = descDao.updateDesc(desc, cra);
		
		readDesc = descDao.readDesc(new DescKey("testId", "testType", 0), cra);
		Assert.assertNotNull(readDesc);
		Assert.assertNotSame("testDescription", readDesc.getDescription());
		Assert.assertEquals("testDescription2", readDesc.getDescription());
		
	}
}
