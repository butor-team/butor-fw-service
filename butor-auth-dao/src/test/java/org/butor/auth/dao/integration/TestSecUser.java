/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupKey;
import org.butor.auth.common.user.ListUserCriteria;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserQuestions;
import org.butor.auth.dao.AuthDao;
import org.butor.auth.dao.GroupDao;
import org.butor.auth.dao.UserDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;


@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecUser {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource
	UserDao userDao;

	@Resource(name="secAuthDao")
	AuthDao authDao;

	@Resource
	GroupDao groupDao;
	
	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {

		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-db.sql"), "TESTDB", "mysql");
	}


	@Test
	public void testReadUserWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecUser.sql"), "TESTDB", "mysql");
		
		User user =  userDao.readUser("unitTest2", null, cra);
		
		Assert.assertNotNull(user);
	}

	@Test
	public void testReadUserWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecUser.sql"), "TESTDB", "mysql");
		
		User user =  userDao.readUser("unitTest", null, cra);
		Assert.assertNull(user);

		user =  userDao.readUser("unitTest2", null, cra);
		Assert.assertNotNull(user);
	
	}

	@Test
	public void testReadAndUpdateQuestions() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecUser.sql"), "TESTDB", "mysql");
		
		
		
		UserQuestions uq = userDao.readQuestions("unitTest", cra);
		Assert.assertNotNull(uq);
		
		Assert.assertFalse((uq.getQ1()).contentEquals("Question 1"));
		Assert.assertFalse((uq.getQ2()).contentEquals("Question 2"));
		Assert.assertFalse((uq.getQ3()).contentEquals("Question 3"));
		Assert.assertFalse((uq.getR1()).contentEquals("Answer 1"));
		Assert.assertFalse((uq.getR2()).contentEquals("Answer 2"));
		Assert.assertFalse((uq.getR3()).contentEquals("Answer 3"));
		
		uq.setQ1("Question 1");
		uq.setQ2("Question 2");
		uq.setQ3("Question 3");
		uq.setR1("Answer 1");
		uq.setR2("Answer 2");
		uq.setR3("Answer 3");
		
		UserKey userKey =  userDao.updateQuestions(uq, cra);
		
		Assert.assertNotNull(userKey);
		
		uq = userDao.readQuestions("unitTest", cra);
		
		Assert.assertTrue((uq.getQ1()).contentEquals("Question 1"));
		Assert.assertTrue((uq.getQ2()).contentEquals("Question 2"));
		Assert.assertTrue((uq.getQ3()).contentEquals("Question 3"));
		Assert.assertTrue((uq.getR1()).contentEquals("Answer 1"));
		Assert.assertTrue((uq.getR2()).contentEquals("Answer 2"));
		Assert.assertTrue((uq.getR3()).contentEquals("Answer 3"));
		
	}

	@Test
	public void testDeleteAndInsert() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecUser.sql"), "TESTDB", "mysql");

		
		User user1 = userDao.readUser("unitTest1", null, cra);
		Assert.assertNotNull(user1);
				
		userDao.deleteUser(new UserKey(user1.getId(), user1.getRevNo()), cra);
		
		User user2 = userDao.readUser("unitTest1", null, cra);
		Assert.assertNull(user2);
		
		UserKey userKey = userDao.insertUser(user1, cra);
		Assert.assertNotNull(userKey);
		
		user2 = userDao.readUser("unitTest1", null, cra);
		Assert.assertNotNull(user2);
		
		user1.setStamp(user2.getStamp());
		user1.setCreationDate(user2.getCreationDate());
		user1.setUserId("unitTest");
		
		Assert.assertEquals(user1, user2);

	}

	
	@Test
	public void testDelete() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecUser.sql"), "TESTDB", "mysql");
		
		// check existing user
		User user1 = userDao.readUser("unitTest2", null, cra);
		Assert.assertNotNull(user1);
		// delete existing user
		userDao.deleteUser(new UserKey(user1.getId(), user1.getRevNo()), cra);
		Assert.assertNull(userDao.readUser(user1.getUserId(), null, cra));

		// check existing group
		List<GroupItem> listGroupItem = groupDao.readGroup("testGroup", cra);
		Assert.assertEquals(1, listGroupItem.size());
		// delete group
		groupDao.deleteGroup(new GroupKey("testGroup"), cra);
		listGroupItem = groupDao.readGroup("testGroup", cra);
		Assert.assertEquals(0, listGroupItem.size());

		// check number of auth
		ListAuthCriteria crit = new ListAuthCriteria();
		List<String> whos = new ArrayList<String>();
		whos.add("unitTest2");
		crit.setWhos(whos);
		List<String> whoTypes = new ArrayList<String>();
		whoTypes.add("user");
		crit.setWhoTypes(whoTypes);
		List<Auth> listAuth = authDao.listAuth(crit, cra);
		Assert.assertEquals(3, listAuth.size());
		// delete first auth
		Auth auth = listAuth.get(0);
		authDao.deleteAuth(new AuthKey(auth.getAuthId(), auth.getRevNo()), cra);
		listAuth = authDao.listAuth(crit, cra);
		Assert.assertEquals(2, listAuth.size());
	}
	
	@Test
	public void testUpdate() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		
		
		
		User user1 = userDao.readUser("unitTest", null, cra);
		Assert.assertNotNull(user1);
		
		user1.setFullName("Test Update FullName");
		
		UserKey userKey = userDao.updateUser(user1, cra);
		
		User user2 = userDao.readUser("unitTest", null, cra);
		Assert.assertNotNull(user2);
		Assert.assertEquals("Test Update FullName", user2.getFullName());
		
		Assert.assertFalse(user2.isResetInProgress());
		
		user2.setResetInProgress(true);
		
		userKey = userDao.updateState(user2, cra);
		Assert.assertNotNull(userKey);
		
		user1 = userDao.readUser("unitTest", null, cra);
		Assert.assertNotNull(user1);
		
		Assert.assertTrue(user1.isResetInProgress());
		
	}
	
	@Test
	public void testListUserWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecUser.sql"), "TESTDB", "mysql");
		
		ListUserCriteria crit = new ListUserCriteria();
		List<User> listUser = userDao.listUser(crit, null, cra); 
		
		Assert.assertEquals(6, listUser.size());

		crit.setFirmId(1);
		
		listUser = userDao.listUser(crit, null, cra);

		Assert.assertEquals(3, listUser.size());
	}

	@Test
	public void testListUserWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecUser.sql"), "TESTDB", "mysql");
		
		ListUserCriteria crit = new ListUserCriteria();
		List<User> listUser = userDao.listUser(crit, null, cra); 
		
		Assert.assertEquals(1, listUser.size());

		crit.setFirmId(1);
		
		listUser = userDao.listUser(crit, null, cra);

		Assert.assertEquals(1, listUser.size());
	}
}
