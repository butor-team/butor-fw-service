/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao.integration;

import static org.butor.test.ButorTestUtil.getCommonRequestArgs;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.butor.auth.common.desc.Desc;
import org.butor.auth.common.func.Func;
import org.butor.auth.common.func.FuncKey;
import org.butor.auth.common.role.Role;
import org.butor.auth.common.role.RoleItem;
import org.butor.auth.common.role.RoleItemKey;
import org.butor.auth.common.role.RoleKey;
import org.butor.auth.dao.DescDao;
import org.butor.auth.dao.FuncDao;
import org.butor.auth.dao.RoleDao;
import org.butor.json.CommonRequestArgs;
import org.butor.test.ButorTestUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.ResourceUtils;


@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:test-context.xml")
public class TestSecRole {

	@BeforeClass
	public static void bootstrap() {
		BasicConfigurator.configure();
	}

	@Resource(name="secDs")
	DataSource dataSource;

	@Resource
	RoleDao roleDao;

	@Resource
	DescDao descDao;

	@Resource
	FuncDao funcDao;

	ButorTestUtil tu = new ButorTestUtil();
	
	@Before
	public void init() throws Exception {
		
		
	}

	@Test
	public void testReadItem() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		
		RoleItem roleItem = roleDao.readItem(new RoleItemKey("sec.admin", "auths", "sec"), cra);
		
		Assert.assertNotNull(roleItem);
	}

	@Test
	public void testReadRoleWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		
		List<RoleItem> roleItem =  roleDao.readRole("sec.admin", cra);
		
		Assert.assertEquals(7, roleItem.size());

		roleItem =  roleDao.readRole("testRole1", cra);
		
		Assert.assertEquals(0, roleItem.size());
	}
	
	@Test
	public void testReadRoleWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		
		List<RoleItem> listRoleItem =  roleDao.readRole("sec.admin", cra);
		
		Assert.assertEquals(2, listRoleItem.size());

	}
	
	
	@Test
	public void testInsertItem() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		RoleItem item = new RoleItem();
		item.setFunc("admin");
		item.setSys("jb");
		item.setRoleId("role1");
		RoleItemKey rik = roleDao.insertItem(item, cra);
		assertNotNull(rik);
		
		RoleItem roleItem = roleDao.readItem(new RoleItemKey("role1", "admin", "jb"), cra);
		
		Assert.assertNotNull(roleItem);
	}
	@Test
	public void testDeleteItem() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		RoleItem item = new RoleItem();
		item.setFunc("admin");
		item.setSys("jb");
		item.setRoleId("role1");
		RoleItemKey rik = roleDao.insertItem(item, cra);
		assertNotNull(rik);

		RoleItem roleItem = roleDao.readItem(new RoleItemKey("role1", "admin", "jb"), cra);
		Assert.assertNotNull(roleItem);

		roleDao.deleteItem(rik, cra);

		roleItem = roleDao.readItem(new RoleItemKey("role1", "admin", "jb"), cra);
		Assert.assertNull(roleItem);
	}

	@Test
	public void testDeleteRole() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");

		List<RoleItem> roleItem =  roleDao.readRole("sec.admin", cra);
		
		Assert.assertEquals(7, roleItem.size());

		roleDao.deleteRole(new RoleKey("sec.admin"), cra);

		roleItem =  roleDao.readRole("sec.admin", cra);
		
		Assert.assertEquals(0, roleItem.size());

	}

	@Test
	public void testUpdateRole() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");

		List<RoleItem> listRoleItem =  roleDao.readRole("sec.admin", cra);
		
		Assert.assertEquals(7, listRoleItem.size());

		Role role = new Role();
		role.setId("sec.admin");
		
		RoleItem roleItem = new RoleItem();
		roleItem.setDescription("Role Item Description");
		roleItem.setRoleId("sec.admin");
		roleItem.setFunc("testFunc");
		roleItem.setSys("testSys");
		
		listRoleItem.add(roleItem);
		
		role.setItems(listRoleItem);		
		
		roleDao.updateRole(role, cra);

		listRoleItem =  roleDao.readRole("sec.admin", cra);
		
		Assert.assertEquals(8, listRoleItem.size());

	}

	@Test
	public void testIsRoleRefered() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");

		boolean isRoleRefered = roleDao.isRoleRefered("sec.admin", cra);
		Assert.assertTrue(isRoleRefered);
		
		isRoleRefered = roleDao.isRoleRefered("testRole", cra);
		Assert.assertFalse(isRoleRefered);
		
	}
	
	@Test
	public void testListItem() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		String roleId = "role1";
		Desc desc = new Desc();
		desc.setDescription("The Group #1");
		desc.setId(roleId);
		desc.setIdType("group");
		descDao.insertDesc(desc, cra);

		int items = 5;
		for (int i=0; i<items; i++) {
			Func func = new Func();
			func.setDescription("Function #1");
			func.setFunc("func-" +i);
			func.setSys("test");
			funcDao.insertFunc(func, cra);

			RoleItem item = new RoleItem();
			item.setFunc(func.getFunc());
			item.setSys(func.getSys());
			item.setRoleId(roleId);
			RoleItemKey rik = roleDao.insertItem(item, cra);
			System.out.println("Inserted role func: " +item);
			assertNotNull(rik);
		}
		List<RoleItem> ril = roleDao.readRole(roleId, cra);
		assertNotNull(ril);
		System.out.println("Got role funcs: " +ril);
		Assert.assertEquals(items, ril.size());
	}
	@Test
	public void testListRole() throws FileNotFoundException, IOException {
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		CommonRequestArgs cra = getCommonRequestArgs("{userId='unitTest'}");
		FuncKey fk = new FuncKey(null,null);
		List<Role> roles = roleDao.listRole(fk, null, cra);
		assertNotNull(roles);
		System.out.println("Got roles: " +roles);
	}
	
	@Test
	public void testListRoleWithAdminAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		
		List<Role> listRole = roleDao.listRole(null, null, cra); 
		
		Assert.assertEquals(4, listRole.size());

		listRole = roleDao.listRole(new FuncKey("auths", "sec"), null, cra);

		Assert.assertEquals(1, listRole.size());
	}

	@Test
	public void testListRoleWithLimitedAccess() throws FileNotFoundException, IOException {
		CommonRequestArgs cra = getCommonRequestArgs("{userId:'unitTest2'}");
		JdbcTemplate jt = new JdbcTemplate(dataSource);
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestAuthDao.sql"), "TESTDB", "mysql");
		tu.executeSQL(jt, ResourceUtils.getURL("classpath:butor-auth-dao-TestSecRole.sql"), "TESTDB", "mysql");
		
		List<Role> listRole = roleDao.listRole(null, null, cra); 
		
		Assert.assertEquals(1, listRole.size());

		listRole = roleDao.listRole(new FuncKey("funcs", "sec"), null, cra);

		Assert.assertEquals(1, listRole.size());
	}

	
}
