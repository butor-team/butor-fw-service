--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

truncate table secDesc;
truncate table secFunc;
truncate table secGroup;
truncate table secRole;
truncate table secData;
truncate table secAuth;
truncate table secUser;

INSERT INTO secUser (`id`,`active`,`firmId`,`creationDate`,`displayName`,`fullName`,`email`,`firstName`,`lastLoginDate`,`lastName`,`pwd`,`resetInProgress`,`missedLogin`,`lastQ`,`revNo`,`stamp`,`userId`) VALUES 
	('unitTest',true,1,CURRENT_TIMESTAMP,'unitTest','unitTest','unitTest@test.tst','unitTest',CURRENT_TIMESTAMP,'unitTest','2e4abcc5f49243072dd33136807cc1f88843fe82e1c33137e6b52acf67145bdbbd4aee68f5dd0faae37a8d0ec3528363102833319ac29252df72e0e54a7dfbc6991091aeae5ae887',false, 0, 0,1,CURRENT_TIMESTAMP,'load');

INSERT INTO secFunc (`func`, `sys`, `description`, `stamp`, `userId`,`revNo`) VALUES 
	('sec','sec','Security', CURRENT_TIMESTAMP, 'load',0),
	('firms','sec','Security firms', CURRENT_TIMESTAMP, 'load',0),
	('users','sec','Security users', CURRENT_TIMESTAMP, 'load',0),
	('funcs','sec','Security functions', CURRENT_TIMESTAMP, 'load',0),
	('roles','sec','Security roles', CURRENT_TIMESTAMP, 'load',0),
	('groups','sec','Security groups', CURRENT_TIMESTAMP, 'load',0),
	('auths','sec','Security authorisations', CURRENT_TIMESTAMP, 'load',0);

	
INSERT INTO secDesc (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','role', 'Security admin role',CURRENT_TIMESTAMP,'load',0);

INSERT INTO secRole (`roleId`,`func`,`sys`,`mode`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','sec','sec',2,CURRENT_TIMESTAMP,'load',0),
	('sec.admin','firms','sec',2,CURRENT_TIMESTAMP,'load',0),
	('sec.admin','users','sec',2,CURRENT_TIMESTAMP,'load',0),
	('sec.admin','groups','sec',2,CURRENT_TIMESTAMP,'load',0),
	('sec.admin','funcs','sec',2,CURRENT_TIMESTAMP,'load',0),
	('sec.admin','roles','sec',2,CURRENT_TIMESTAMP,'load',0),
	('sec.admin','auths','sec',2,CURRENT_TIMESTAMP,'load',0);

INSERT INTO secDesc (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','group', 'Security admin group',CURRENT_TIMESTAMP,'load',0);

INSERT INTO secGroup (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','unitTest',CURRENT_TIMESTAMP,'load',0);

INSERT INTO secAuth (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec.admin','group','sec.admin','role','sec',1,2,null,null,CURRENT_TIMESTAMP,'load',0);


INSERT INTO secData (`dataId`,`sys`,`dataType`,`d1`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec','firm','*',CURRENT_TIMESTAMP,'load',0),
	(1,'sec','user','*',CURRENT_TIMESTAMP,'load',0),
	(1,'sec','group','*',CURRENT_TIMESTAMP,'load',0),
	(1,'sec','role','*',CURRENT_TIMESTAMP,'load',0),
	(1,'sec','func','*',CURRENT_TIMESTAMP,'load',0);

select * from secData;

