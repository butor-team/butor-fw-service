--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--


INSERT INTO secAuth (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(2, 'testGroup1','group','testRole1','role','',-1,1,null,null,CURRENT_TIMESTAMP,'load',0),
	(3, 'testGroup2','group','testFunction1','func','sec',-1,1,null,null,NOW(),'load',0),
	(4, 'testUser1','user','sec.admin','role','',2,1,null,null,NOW(),'load',0),
	(5, 'testUser2','user','testFunction2','func','testSys1',2,1,'2012-02-01 11:11:11','2013-01-29 14:14:11',NOW(),'load',0),
	(6, 'unitTest','user','funcTest','func','sysTest',-1,1,null,null,NOW(),'load',0);


INSERT INTO secData (`dataId`,`sys`,`dataType`,`d1`,d2,`stamp`,`userId`,`revNo`) VALUES
	(2,'sec','user','unitTest','',NOW(),'load',0),
	(2,'sec','func','sysTest', '*',NOW(),'load',0),
	(2,'sec','role','sec.admin','', NOW(),'load',0);
	