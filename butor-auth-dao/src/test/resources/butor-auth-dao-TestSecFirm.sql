--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

truncate secFirm;

INSERT INTO secFirm (`active`,`theme`,`firmId`,`firmName`,`contactName`,`contactPhone`, `attributes`, `creationDate`,`revNo`,`stamp`,`userId`) VALUES 
	(true,'TMCF21E6A7',1,'TestFirm1','Contact Name 1','514 ...', null, NOW(),1,CURRENT_TIMESTAMP,'load'),
	(false,'TMCF21E6A7',2,'TestFirm2','Contact Name 2','514 ...',null, NOW(),1,CURRENT_TIMESTAMP,'load'),
	(true,'TMCF21E6A7',3,'TestFirm3','Contact Name 3','514 ...',null, NOW(),1,CURRENT_TIMESTAMP,'load'),
	(false,'TMCF21E6A7',4,'TestFirm4','Contact Name 4','514 ...',null, NOW(),1,CURRENT_TIMESTAMP,'load'),
	(true,'TMCF21E6A7',5,'TestFirm5','Contact Name 5','514 ...',null, NOW(),1,CURRENT_TIMESTAMP,'load');

INSERT INTO secAuth (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(2,'unitTest2','user','firms','func','sec',2,2,null,null,NOW(),'load',0);


INSERT INTO secData (`dataId`,`sys`,`dataType`,`d1`,`stamp`,`userId`,`revNo`) VALUES
	(2,'sec','firm','1',NOW(),'load',0),
	(2,'sec','firm','3',NOW(),'load',0),
	(2,'sec','firm','5',NOW(),'load',0),
	(2,'sec','user','*',NOW(),'load',0),
	(2,'sec','sys','*',NOW(),'load',0),
	(2,'sec','group','*',NOW(),'load',0),
	(2,'sec','role','*',NOW(),'load',0),
	(2,'sec','func','*',NOW(),'load',0);