--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

INSERT INTO secDesc (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('testGroup1','group', 'Test group 1',NOW(),'load',0),
	('testGroup2','group', 'Test group 2',NOW(),'load',0),
	('testGroup3','group', 'Test group 3',NOW(),'load',0);
	

insert INTO secGroup (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('testGroup1','testMember1',NOW(),'load',0),
	('testGroup1','testMember2',NOW(),'load',0),
	('testGroup1','testMember3',NOW(),'load',0),
	('testGroup1','testMember4',NOW(),'load',0),
	('testGroup1','testMember5',NOW(),'load',0),
	('testGroup1','testMember6',NOW(),'load',0),
	('testGroup1','testMember7',NOW(),'load',0),
	('testGroup1','testMember8',NOW(),'load',0),
	('testGroup3','testMember1',NOW(),'load',0);

INSERT INTO secAuth (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(2,'unitTest2','user','groups','func','sec',2,2,null,null,NOW(),'load',0);


INSERT INTO secData (`dataId`,`sys`,`dataType`,`d1`,`stamp`,`userId`,`revNo`) VALUES
	(2,'sec','group','testGroup1',NOW(),'load',0),
	(2,'sec','group','testGroup2',NOW(),'load',0),
	(2,'sec','user','testMember1',NOW(),'load',0),
	(2,'sec','user','testMember2',NOW(),'load',0),
	(2,'sec','user','testMember3',NOW(),'load',0),
	(2,'sec','user','testMember4',NOW(),'load',0);
