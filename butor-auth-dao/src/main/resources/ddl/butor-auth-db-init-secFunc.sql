--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- security functions ---------------

/* 
 * remove comment when all the function are moved here.
 * probably remove the database name from the sql statement. 
 * Just run on the right database e.g. : mysql -Dmydatabase parameter.
*/

-- DELETE FROM `PORTAL`.`secFunc` WHERE sys="sec";

INSERT INTO `PORTAL`.`secFunc` (`func`, `sys`, `description`, `stamp`, `userId`,`revNo`) VALUES 
	('audit', 'sec', 'Security audit', NOW(), 'load',0);

	
INSERT INTO `PORTAL`.`secRole` (`roleId`,`func`,`sys`,`mode`, `stamp`,`userId`,`revNo`) VALUES
	 ('sec.admin','audit','sec',2,NOW(),'load',0)
;

