--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT 
  sc.user,
  sc.namespace,
  sc.service,
  sc.payload,
  sc.success,
  sc.duration,
  sc.serviceCallTimestamp,
  sc.reqId,
  sc.sessionId,
  sc.stamp,
  sc.revNo,
  sc.userId
FROM serviceCall sc
left join secUser u on
  u.id = sc.user
left join (__authDataSql__) dfu on
  (dfu.dataType = 'firm' and
  (dfu.d1 = '*' or dfu.d1 = u.firmId)) or
  (dfu.dataType = 'user' and
  (dfu.d1 = '*' or dfu.d1 = u.id))
LEFT JOIN secFirm c ON
  c.firmId = u.firmId
WHERE
  dfu.d1 is not null AND
  (sc.serviceCallTimestamp BETWEEN :dateFrom AND :dateTo) AND
  (:user is null or sc.user LIKE CONCAT('%',:user,'%')) AND
  (:session is null or sc.sessionId = :session) AND
  (:service is null or sc.service = :service)
ORDER BY sc.id DESC
  limit :limit
