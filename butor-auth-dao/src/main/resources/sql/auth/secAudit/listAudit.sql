--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- show only login occurrences (different lastLoginDate)
-- ignore user info updates
select distinct data.* from
(
/* last login from secUser table */
SELECT distinct
  u.id,
  u.displayName,
  u.lastLoginDate,
  u.attributes,
  c.firmName
FROM secUser u
left join (__authDataSql__) dfu on
  (dfu.dataType = 'firm' and
  (dfu.d1 = '*' or dfu.d1 = u.firmId)) or
  (dfu.dataType = 'user' and
  (dfu.d1 = '*' or dfu.d1 = u.id))
LEFT JOIN secFirm c ON
  c.firmId = u.firmId  
WHERE
  dfu.d1 is not null AND
  (u.lastLoginDate IS NOT NULL AND u.lastLoginDate BETWEEN :dateFrom AND :dateTo) AND 
  (:displayName IS NULL OR u.displayName LIKE CONCAT('%',:displayName,'%')) AND 
  (:id IS NULL OR u.id LIKE CONCAT('%',:id,'%'))
  
UNION

/* previous (historical) logins from secUserHist table */
SELECT distinct
  u.id,
  u.displayName,
  u.lastLoginDate,
  u.attributes,
  c.firmName
FROM secUserHist u
left join (__authDataSql__) dfu on
  (dfu.dataType = 'firm' and
  (dfu.d1 = '*' or dfu.d1 = u.firmId)) or
  (dfu.dataType = 'user' and
  (dfu.d1 = '*' or dfu.d1 = u.id))
LEFT JOIN secFirm c ON
  c.firmId = u.firmId  
WHERE
  dfu.d1 is not null AND
  (u.lastLoginDate IS NOT NULL AND u.lastLoginDate BETWEEN :dateFrom AND :dateTo) AND
  (:displayName IS NULL OR u.displayName LIKE CONCAT('%',:displayName,'%')) AND
  (:id IS NULL OR u.id LIKE CONCAT('%',:id,'%'))
) as data
GROUP BY lastLoginDate
ORDER BY lastLoginDate DESC
