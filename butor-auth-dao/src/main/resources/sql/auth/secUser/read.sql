--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT
	u.*,
	f.firmName,
	f.theme
FROM secUser u
left join (__authDataSql__) dfu on
	dfu.dataType = 'firm' and
	(dfu.d1 = '*' or (dfu.d1 = u.firmId))
left join (__authDataSql__) du on
	du.dataType = 'user' and
	(du.d1 = '*' or (du.d1 = u.id))
LEFT JOIN secFirm f ON 
	f.firmId = u.firmId
WHERE
	(dfu.d1 is not null or du.d1 is not null or u.id = :member) and
	u.id = :id
	 
