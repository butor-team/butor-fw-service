--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

INSERT INTO secUser (
	id,
	active,
	firmId,
	creationDate,
	displayName,
	fullName,
	email,
	phone,
	firstName,
	lastLoginDate,
	lastName,
	pwd,
	revNo,
	stamp,
	userId,
	resetInProgress,
	missedLogin,
	lastQ,
	q1,
	r1,
	q2,
	r2,
	q3,
	r3,
	avatar,
	attributes
) VALUES (
	:id,
	:active,
	:firmId,
	:creationDate,
	:displayName,
	:fullName,
	:email,
	:phone,
	:firstName,
	:lastLoginDate,
	:lastName,
	:pwd,
	:revNo,
	:stamp,
	:userId,
	:resetInProgress,
	:missedLogin,
	:lastQ,
	:q1,
	:r1,
	:q2,
	:r2,
	:q3,
	:r3,
	:avatar,
	:attributes
)
