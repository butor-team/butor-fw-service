--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT
	r.roleId,
	r.func,
	f.description,
	r.sys,
	r.mode,
	r.stamp,
	r.revNo,
	r.userId
FROM secRole r
left join secFunc f on 
r.func = f.func and
r.sys = f.sys
where r.roleId = :roleId
	and r.func = :funcId
	and r.sys = :funcSys
