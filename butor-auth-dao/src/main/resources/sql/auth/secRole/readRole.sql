--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT
	r.roleId,
	r.func,
	f.description,
	r.sys,
	r.mode,
	r.stamp,
	r.revNo,
	r.userId
FROM secRole r
inner join (__authDataSql__) dr on
	dr.dataType = 'role' and
	(dr.d1 = '*' or dr.d1 = r.roleId)
left join secFunc f on
	f.sys = r.sys and
	f.func = r.func
inner join (__authDataSql__) df on
	df.dataType = 'func' and
	(df.d1 = '*' or (df.d1 = r.sys and (df.d2 = '*' or df.d2 = r.func)))
where r.roleId = :roleId
order by r.sys 
