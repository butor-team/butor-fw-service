--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

SELECT distinct
	sd.id,
	sd.description,
	sd.stamp,
	if (uu.displayName is null, sd.userId, concat(ifnull(uu.displayName,''), ' (', sd.userId, ')')) as userId,
	sd.revNo
FROM secDesc sd
left join secGroup sg on
	sd.id = sg.groupId
left join secUser uu on
	uu.id = sd.userId
inner join (__authDataSql__) dg on
	dg.dataType = 'group' and
	(dg.d1 = '*' or dg.d1 = sd.id)
where sd.idType = 'group'
and (:groupMember IS NULL OR sg.member = :groupMember)
