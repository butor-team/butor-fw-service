/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.func.FuncKey;
import org.butor.auth.common.role.Role;
import org.butor.auth.common.role.RoleItem;
import org.butor.auth.common.role.RoleItemKey;
import org.butor.auth.common.role.RoleKey;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.ArgsBuilder;
import org.butor.utils.CommonMessageID;

import com.google.api.client.util.Strings;

public class RoleDaoImpl extends AbstractDao implements RoleDao {

	private final String PROC_LIST_ROLE = getClass().getName() +".listRole";
	private final String PROC_READ_ROLE = getClass().getName() +".readRole";
	private final String PROC_READ_ITEM = getClass().getName() +".readItem";
	private final String PROC_INSERT_ITEM = getClass().getName() +".insertItem";
	private final String PROC_IS_ROLE_REFERED = getClass().getName() +".isRoleRefered";
	private final String PROC_DELETE_ROLE = getClass().getName() +".deleteRole";
	private final String PROC_DELETE_ITEM = getClass().getName() +".deleteItem";

	private String listRoleSql;
	private String readRoleSql;
	private String readItemSql;
	private String deleteRoleSql;
	private String deleteItemSql;
	private String isRoleReferedSql;
	private String insertItemSql;

	@Override
	public List<Role> listRole(FuncKey criteria, String func, CommonRequestArgs cra) {
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(Strings.isNullOrEmpty(func) ? SecurityConstants.SEC_FUNC_ROLES : func);
		authData.setDataTypes("role");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());
		
		if (criteria == null){
			criteria = new FuncKey(null, null);
		}
		Map<String, Object> args = ArgsBuilder.create().
			set("funcSys", criteria != null ? criteria.getSys() : null).
			set("funcId", criteria != null ? criteria.getFunc() : null).build();
		
		return queryList(PROC_LIST_ROLE, listRoleSql, Role.class, args, cra, authData);
	}

	public void setListRoleSql(String listSql_) {
		this.listRoleSql = listSql_;
	}

	public void setReadRoleSql(String readSql) {
		this.readRoleSql = readSql;
	}

	@Override
	public RoleItem readItem(RoleItemKey rik, CommonRequestArgs cra) {

		Map<String, Object> args = ArgsBuilder.create().set("roleId", rik.getRoleId()).set("funcSys", rik.getSys()).set("funcId", rik.getFunc()).build();

		return queryFirst(PROC_READ_ITEM, this.readItemSql, RoleItem.class, args, cra);
	}
	@Override
	public List<RoleItem> readRole(String roleId, CommonRequestArgs cra) {
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(SecurityConstants.SEC_FUNC_ROLES);
		authData.setDataTypes("role", "func");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("roleId", roleId);

		return queryList(PROC_READ_ROLE, this.readRoleSql, RoleItem.class, args, cra, authData);
	}

	@Override
	public RoleItemKey insertItem(RoleItem item, CommonRequestArgs cra) {
		item.setStamp(new Date());
		item.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_ITEM, insertItemSql, item, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new RoleItemKey(item.getRoleId(), item.getFunc(), item.getSys(), 0);
	}

	@Override
	public boolean isRoleRefered(String roleId, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("roleId", roleId);
		int refs = queryForInt(PROC_IS_ROLE_REFERED, isRoleReferedSql, args, cra);
		return refs > 0;
	}

	@Override
	public void deleteRole(RoleKey roleKey, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("roleId", roleKey.getId());
		UpdateResult ur = delete(PROC_DELETE_ROLE, deleteRoleSql, args, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}
	@Override
	public void updateRole(Role role, CommonRequestArgs cra) {
		List<RoleItem> funcs = role.getItems();
		List<RoleItem> oldFuncs = readRole(role.getId(), cra);
		Iterator<RoleItem> it = oldFuncs.iterator();
		while (it.hasNext()) {
			boolean funcRemoved = true;
			RoleItem ori = it.next();
			for (RoleItem ri : funcs) {
				if (ri.getFunc().equalsIgnoreCase(ori.getFunc()) && ri.getSys().equalsIgnoreCase(ori.getSys())) {
					funcRemoved = false;
					continue; // role still exists
				}
			}
			if (funcRemoved) {
				RoleItemKey rik = new RoleItemKey(role.getId(), ori.getFunc(), ori.getSys(), ori.getRevNo());
				deleteItem(rik, cra);
				it.remove();
			}
		}
		for (RoleItem ri : funcs) {
			boolean funcAdded = true;
			for (RoleItem ori : oldFuncs) {
				if (ri.getFunc().equalsIgnoreCase(ori.getFunc()) && ri.getSys().equalsIgnoreCase(ori.getSys())) {
					funcAdded = false;
					continue; // role still exists
				}
			}
			if (funcAdded) {
				ri.setRoleId(role.getId());
				RoleItemKey uk = insertItem(ri, cra);
				if (uk == null) {
					ApplicationException.exception(
							CommonMessageID.SERVICE_FAILURE.getMessage());
					return;
				}
			}
		}
	}

	@Override
	public void deleteItem(RoleItemKey rik, CommonRequestArgs cra) {
		UpdateResult ur = delete(PROC_DELETE_ITEM, deleteItemSql, rik, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}

	public void setIsRoleReferedSql(String isRoleReferedSql) {
		this.isRoleReferedSql = isRoleReferedSql;
	}

	public void setInsertItemSql(String insertSql) {
		this.insertItemSql = insertSql;
	}

	public void setDeleteRoleSql(String deleteRoleSql) {
		this.deleteRoleSql = deleteRoleSql;
	}


	public void setDeleteItemSql(String deleteItemSql) {
		this.deleteItemSql = deleteItemSql;
	}

	public String getInsertItemSql() {
		return insertItemSql;
	}

	public void setReadItemSql(String readItemSql) {
		this.readItemSql = readItemSql;
	}
}