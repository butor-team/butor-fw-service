/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthData;
import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.common.ListAuthDataCriteria;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.AuthFunc;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.auth.ListUserAuthFuncCriteria;
import org.butor.auth.common.auth.UserAuthFunc;
import org.butor.auth.common.func.Func;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;

import com.google.common.base.Strings;

public class AuthDaoImpl extends AbstractDao implements AuthDao {

	private String listAuthFuncSql;
	private String listAuthDataSql;
	private String readAuthSql;
	private String listAuthSql;
	private String listUserAuthFuncSql;
	private String insertAuthSql;
	private String updateAuthSql;
	private String deleteAuthSql;
	private String hasAuthFuncSql;

	private final String PROC_LIST_AUTH_FUNC = getClass().getName() +".listAuthFunc";
	private final String PROC_HAS_ACCESS = getClass().getName() +".hasAccess";
	private final String PROC_LIST_AUTH_DATA = getClass().getName() +".listAuthData";
	private final String PROC_LIST_AUTH = getClass().getName() +".listAuth";
	private final String PROC_LIST_USER_AUTH_FUNC = getClass().getName() +".listUserAuthFunc";
	private final String PROC_READ_AUTH = getClass().getName() +".readAuth";
	private final String PROC_UPDATE_AUTH = getClass().getName() +".updateAuth";
	private final String PROC_INSERT_AUTH = getClass().getName() +".insertAuth";
	private final String PROC_DELETE_AUTH = getClass().getName() +".deleteAuth";

	@Override
	public List<Func> listAuthFunc(CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("member", cra.getUserId());
		return queryList(PROC_LIST_AUTH_FUNC, this.listAuthFuncSql, Func.class, args, cra);
	}

	@Override
	public boolean hasAccess(String system, String func, AccessMode am, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("sys", system);
		args.put("func", func);
		args.put("member", cra.getUserId());
		args.put("mode", am.value());
		List<AuthFunc> x = queryList(PROC_HAS_ACCESS, this.hasAuthFuncSql, AuthFunc.class, args);
		return x.size() > 0;
	}

	@Override
	public List<AuthData> listAuthData(ListAuthDataCriteria criteria, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("mode", criteria.getAccessMode() != null ? criteria.getAccessMode().value() : null);
		args.put("member", cra.getUserId());
		return queryList(PROC_LIST_AUTH_DATA, this.listAuthDataSql, AuthData.class, criteria, args, cra);
	}

	@Override
	public List<Auth> listAuth(ListAuthCriteria criteria, CommonRequestArgs cra) {
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(SecurityConstants.SEC_FUNC_AUTHS);
		authData.setDataTypes("firm", "user", "role", "func", "group");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());
		
		return queryList(PROC_LIST_AUTH, this.listAuthSql, Auth.class, criteria, cra, authData);
	}

	@Override
	public List<UserAuthFunc> listUserAuthFunc(ListUserAuthFuncCriteria criteria, CommonRequestArgs cra) {
		return queryList(PROC_LIST_USER_AUTH_FUNC, this.listUserAuthFuncSql, UserAuthFunc.class, criteria, cra);
	}

	public void setListAuthFuncSql(String listAuthFuncSql) {
		this.listAuthFuncSql = listAuthFuncSql;
	}

	public void setListAuthDataSql(String listAuthDataSql) {
		this.listAuthDataSql = listAuthDataSql;
	}

	@Override
	public Map<String, List<String>> prepareAuthData(String system, String func, AccessMode am, CommonRequestArgs cra,
			String... dataTypes) {
		if (Strings.isNullOrEmpty(system) || Strings.isNullOrEmpty(func) || am == null || cra == null
				|| dataTypes == null || dataTypes.length == 0) {
			logger.warn(String.format("Missing ListAuthDataCriteria! Got system=%s, func=%s, AccessMode=%s, "
					+ "CommonRequestArgs=%s, dataTypes=%s", system, func, am, cra, dataTypes));
			ApplicationException.exception(DAOMessageID.UNAUTHORIZED.getMessage());
		}

		ListAuthDataCriteria authDataCrit = new ListAuthDataCriteria();
		authDataCrit.setSys(system).setAccessMode(am).setFunc(func).setDataTypes(dataTypes);

		List<AuthData> authDataList = listAuthData(authDataCrit, cra);
		if (authDataList.size() == 0) {
			logger.info(String.format("No auth data found! with criteria system=%s, func=%s, AccessMode=%s, "
					+ "CommonRequestArgs=%s, dataTypes=%s", system, func, am, cra, dataTypes));
		}

		// split authorised data by type
		Map<String, List<String>> dataCrit = new HashMap<String, List<String>>();
		for (AuthData ad : authDataList) {
			String key = ad.getDataType() + "_list";
			List<String> list = dataCrit.get(key);
			if (list == null) {
				list = new ArrayList<String>();
				dataCrit.put(key, list);
			}
			String data = ad.getD1();
			if (!Strings.isNullOrEmpty(ad.getD2())) {
				data += "." + ad.getD2();
			}
			if (!Strings.isNullOrEmpty(ad.getD3())) {
				data += "." + ad.getD3();
			}
			if (!Strings.isNullOrEmpty(ad.getD4())) {
				data += "." + ad.getD4();
			}
			if (!Strings.isNullOrEmpty(ad.getD5())) {
				data += "." + ad.getD5();
			}
			list.add(data);
		}
		// data types lists MUST exists at least with null value in the result.
		// required so query conditions referring to those lists
		// through sql placeholders whould be happy.
		// example: ... AND firmId IN (:firmList) ...
		// if "firmList" does not exists, there will be SQL error.
		for (String dt : dataTypes) {
			String key = dt + "_list";
			if (!dataCrit.containsKey(key)) {
				dataCrit.put(key, null);
			}
		}

		if (logger.isDebugEnabled()) {
			logger.info(String.format("Found auth data with criteria system=%s, func=%s, AccessMode=%s, "
					+ "CommonRequestArgs=%s, dataTypes=%s, data=%s", system, func, am, cra, dataTypes, dataCrit));

		}
		return dataCrit;
	}

	public void setHasAuthFuncSql(String hasAuthFuncSql) {
		this.hasAuthFuncSql = hasAuthFuncSql;
	}

	public void setListAuthSql(String listAuthSql) {
		this.listAuthSql = listAuthSql;
	}

	public void setListUserAuthFuncSql(String listUserAuthFuncSql) {
		this.listUserAuthFuncSql = listUserAuthFuncSql;
	}

	@Override
	public Auth readAuth(long authId, CommonRequestArgs cra) {
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(SecurityConstants.SEC_FUNC_AUTHS);
		authData.setDataTypes("firm", "user", "role", "func", "group");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("authId", authId);
		return queryFirst(PROC_READ_AUTH, this.readAuthSql, Auth.class, args, cra, authData);
	}

	@Override
	public AuthKey updateAuth(Auth auth, CommonRequestArgs cra) {
		UpdateResult ur = update(PROC_UPDATE_AUTH, updateAuthSql, auth, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new AuthKey(auth.getAuthId(), auth.getRevNo() + 1);
	}

	@Override
	public AuthKey insertAuth(Auth auth, CommonRequestArgs cra) {
		auth.setAuthId(null);
		auth.setStamp(new Date());
		auth.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_AUTH, insertAuthSql, auth, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage());
		}
		return new AuthKey(ur.key.intValue(), 0); //id is int in table
	}

	@Override
	public void deleteAuth(AuthKey ak, CommonRequestArgs cra) {
		UpdateResult ur = delete(PROC_DELETE_AUTH, deleteAuthSql, ak, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}

	public void setReadAuthSql(String readAuthSql) {
		this.readAuthSql = readAuthSql;
	}

	public void setUpdateAuthSql(String updateAuthSql) {
		this.updateAuthSql = updateAuthSql;
	}

	public void setDeleteAuthSql(String deleteAuthSql) {
		this.deleteAuthSql = deleteAuthSql;
	}

	public void setInsertAuthSql(String insertAuthSql) {
		this.insertAuthSql = insertAuthSql;
	}

	public String getInsertAuthSql() {
		return insertAuthSql;
	}
}