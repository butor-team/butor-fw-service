/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.List;

import org.butor.auth.common.func.FuncKey;
import org.butor.auth.common.role.Role;
import org.butor.auth.common.role.RoleItem;
import org.butor.auth.common.role.RoleItemKey;
import org.butor.auth.common.role.RoleKey;
import org.butor.json.CommonRequestArgs;

public interface RoleDao {
	RoleItem readItem(RoleItemKey rk, CommonRequestArgs cra);
	List<RoleItem> readRole(String roleId, CommonRequestArgs cra);
	RoleItemKey insertItem(RoleItem item, CommonRequestArgs cra);
	void deleteItem(RoleItemKey rik, CommonRequestArgs cra);
	void deleteRole(RoleKey roleKey, CommonRequestArgs cra);
	void updateRole(Role role, CommonRequestArgs cra);
	boolean isRoleRefered(String roleId, CommonRequestArgs cra);
	List<Role> listRole(FuncKey criteria, String func, CommonRequestArgs cra);
}
