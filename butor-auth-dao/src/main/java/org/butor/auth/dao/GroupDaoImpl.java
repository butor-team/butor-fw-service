/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.butor.auth.common.AuthDataFilter;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupItemKey;
import org.butor.auth.common.group.GroupKey;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;

import com.google.common.base.Strings;

public class GroupDaoImpl extends AbstractDao implements GroupDao  {

	private final String PROC_LIST_GROUP = getClass().getName() +".listGroup";
	private final String PROC_READ_ITEM = getClass().getName() +".readItem";
	private final String PROC_READ_GROUP = getClass().getName() +".readGroup";
	private final String PROC_INSERT_ITEM = getClass().getName() +".insertItem";
	private final String PROC_IS_GROUP_REFERED = getClass().getName() +".isGroupRefered";
	private final String PROC_DELETE_GROUP = getClass().getName() +".deleteGroup";
	private final String PROC_DELETE_ITEM = getClass().getName() +".deleteItem";

	private String listGroupSql;
	private String readGroupSql;
	private String readItemSql;
	private String deleteGroupSql;
	private String deleteItemSql;
	private String isGroupReferedSql;
	private String insertItemSql;

	@Override
	public List<Group> listGroup(String member, String func, CommonRequestArgs cra) {
		
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(Strings.isNullOrEmpty(func) ? SecurityConstants.SEC_FUNC_GROUPS : func);
		authData.setDataTypes("group");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("groupMember", Strings.isNullOrEmpty(member) ? null : member);
		
		return queryList(PROC_LIST_GROUP, listGroupSql, Group.class, args, cra, authData);
	}

	public void setListGroupSql(String listSql_) {
		this.listGroupSql = listSql_;
	}

	public void setReadGroupSql(String readSql) {
		this.readGroupSql = readSql;
	}

	@Override
	public GroupItem readItem(GroupItemKey gik, CommonRequestArgs cra) {
		return queryFirst(PROC_READ_ITEM, this.readItemSql, GroupItem.class, gik, cra);
	}
	@Override
	public List<GroupItem> readGroup(String groupId, CommonRequestArgs cra) {
		
		AuthDataFilter authData = new AuthDataFilter();
		authData.setSys(SecurityConstants.SYSTEM_ID);
		authData.setFunc(SecurityConstants.SEC_FUNC_GROUPS);
		authData.setDataTypes("group", "firm", "user");
		authData.setMode((AccessMode.READ).value());
		authData.setMember(cra.getUserId());

		Map<String, Object> args = new HashMap<String, Object>();
		args.put("groupId", groupId);
		return queryList(PROC_READ_GROUP, this.readGroupSql, GroupItem.class, args, cra, authData);
	}
	@Override
	public GroupItemKey insertItem(GroupItem item, CommonRequestArgs cra) {
		item.setStamp(new Date());
		item.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_ITEM, insertItemSql, item, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new GroupItemKey(item.getGroupId(), item.getMember(), 0);
	}

	@Override
	public boolean isGroupRefered(String groupId, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("groupId", groupId);
		int refs = queryForInt(PROC_IS_GROUP_REFERED, isGroupReferedSql, args, cra);
		return refs > 0;
	}

	@Override
	public void updateGroup(Group group, CommonRequestArgs cra) {
		List<GroupItem> members = group.getItems();
		group.setIdType("group");

		List<GroupItem> oldMembers = readGroup(group.getId(), cra);
		Iterator<GroupItem> it = oldMembers.iterator();
		while (it.hasNext()) {
			boolean memberRemoved = true;
			GroupItem ogi = it.next();
			for (GroupItem gi : members) {
				if (gi.getMember().equalsIgnoreCase(ogi.getMember())) {
					memberRemoved = false;
					continue; // member still exists
				}
			}
			if (memberRemoved) {
				GroupItemKey gik = new GroupItemKey(group.getId(), ogi.getMember(), ogi.getRevNo());
				deleteItem(gik, cra);
				it.remove();
			}
		}
		for (GroupItem gi : members) {
			boolean memberAdded = true;
			for (GroupItem ogi : oldMembers) {
				if (gi.getMember().equalsIgnoreCase(ogi.getMember())) {
					memberAdded = false;
					continue; // member still exists
				}
			}
			if (memberAdded) {
				gi.setGroupId(group.getId());
				GroupItemKey uk = insertItem(gi, cra);
				if (uk == null) {
					ApplicationException.exception(
							CommonMessageID.SERVICE_FAILURE.getMessage());
					return;
				}
			}
		}
	}
	@Override
	public void deleteGroup(GroupKey groupKey, CommonRequestArgs cra) {
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("groupId", groupKey.getId());
		UpdateResult ur = delete(PROC_DELETE_GROUP, deleteGroupSql, args, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}

	@Override
	public void deleteItem(GroupItemKey rik, CommonRequestArgs cra) {
		UpdateResult ur = delete(PROC_DELETE_ITEM, deleteItemSql, rik, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}

	public void setIsGroupReferedSql(String isGroupReferedSql) {
		this.isGroupReferedSql = isGroupReferedSql;
	}

	public void setInsertItemSql(String insertSql) {
		this.insertItemSql = insertSql;
	}
	public String getInsertItemSql() {
		return this.insertItemSql;
	}

	public void setDeleteGroupSql(String deleteGroupSql) {
		this.deleteGroupSql = deleteGroupSql;
	}

	public void setDeleteItemSql(String deleteItemSql) {
		this.deleteItemSql = deleteItemSql;
	}

	public String getReadItemSql() {
		return readItemSql;
	}

	public void setReadItemSql(String readItemSql) {
		this.readItemSql = readItemSql;
	}
}