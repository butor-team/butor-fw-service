/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.List;

import org.butor.auth.common.user.ListUserCriteria;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserQuestions;
import org.butor.json.CommonRequestArgs;

public interface UserDao {

	User readUser(String id, String func, CommonRequestArgs cra);
	UserQuestions readQuestions(String id, CommonRequestArgs cra);

	List<User> readUsers(List<String> idl, CommonRequestArgs cra);
	
	UserKey insertUser(User user, CommonRequestArgs cra);

	void deleteUser(UserKey userKey, CommonRequestArgs cra);

	UserKey updateState(User user, CommonRequestArgs cra);
	UserKey updateUser(User user, CommonRequestArgs cra);
	UserKey updateQuestions(UserQuestions user, CommonRequestArgs cra);

	List<User> listUser(ListUserCriteria criteria, String func, CommonRequestArgs cra);
	List<String> listUserRecentPwd(String id, int maxRows, CommonRequestArgs cra);
}
