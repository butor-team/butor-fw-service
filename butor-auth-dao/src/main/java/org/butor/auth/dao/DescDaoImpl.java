/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.dao;

import java.util.Date;

import org.butor.auth.common.desc.Desc;
import org.butor.auth.common.desc.DescKey;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.ApplicationException;

public class DescDaoImpl extends AbstractDao implements DescDao {
	private String readSql;
	private String deleteSql;
	private String updateSql;
	private String insertSql;

	private final String PROC_READ_DESC = getClass().getName() +".readDesc";
	private final String PROC_INSERT_DESC = getClass().getName() +".insertDesc";
	private final String PROC_UPDATE_DESC = getClass().getName() +".updateDesc";
	private final String PROC_DELETE_DESC = getClass().getName() +".deleteDesc";

	public void setReadSql(String readSql) {
		this.readSql = readSql;
	}

	@Override
	public Desc readDesc(DescKey descKey, CommonRequestArgs cra) {
		return queryFirst(PROC_READ_DESC, this.readSql, Desc.class, descKey, cra);
	}

	@Override
	public DescKey insertDesc(Desc desc, CommonRequestArgs cra) {
		desc.setStamp(new Date());
		desc.setRevNo(0);
		UpdateResult ur = insert(PROC_INSERT_DESC, this.insertSql, desc, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new DescKey(desc.getId(), desc.getIdType(), 0);
	}

	@Override
	public DescKey updateDesc(Desc desc, CommonRequestArgs cra) {
		UpdateResult ur = update(PROC_UPDATE_DESC, this.updateSql, desc, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
		return new DescKey(desc.getId(), desc.getIdType(), desc.getRevNo()+1);
	}

	@Override
	public void deleteDesc(DescKey descKey, CommonRequestArgs cra) {
		UpdateResult ur = delete(PROC_DELETE_DESC, this.deleteSql, descKey, cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage());
		}
	}

	public void setUpdateSql(String updateSql) {
		this.updateSql = updateSql;
	}

	public void setInsertSql(String insertSql) {
		this.insertSql = insertSql;
	}

	public void setDeleteSql(String deleteSql) {
		this.deleteSql = deleteSql;
	}

	public String getInsertSql() {
		return insertSql;
	}
}