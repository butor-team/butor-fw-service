/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.Date;
import java.util.List;

import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.password.PasswordPolicies;
import org.butor.auth.common.password.PasswordValidator;
import org.butor.auth.dao.UserDao;
import org.butor.checksum.CommonChecksumFunction;
import org.butor.json.CommonRequestArgs;
import org.butor.json.util.CommonRequestArgsFactory;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class DefaultPasswordValidator implements PasswordValidator {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private static final long MS_PER_DAY = 1000*60*60*24;
	
	// 8 chars long. lower and upper case letters. numbers. special chars.
	public static final String DEFAULT_PWD_RULES_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$^+=!*()@%&~'`\"\\-_\\\\/?<>;:\\[\\]{}]).{8,}$";

	protected int recentPwdsCount = 15;
	protected int pwdMaxAgeDays = 45;
	protected String pwdRulesRegEx = DEFAULT_PWD_RULES_REGEX;
	protected String pwdRulesPopoverMsgFr = "Choisissez un mot de passe solide : <ul>"
			+ "<li>un minimum de 8 caractères</li>"
			+ "<li>des lettres minuscules et majuscules</li>"
			+ "<li>des chiffres</li>"
			+ "<li>des symboles : #$^+=!*()@%&~'\"\\-_/?<>;:[]{}</li>"
			+ "<li>Le nouveau mot de passe doit être différent des 15 derniers mot de passe.";
	protected String pwdRulesPopoverMsgEn = "Choose a strong password:<ul>"
			+ "<li>Minimum of 8 characters long</li>"
			+ "<li>Lower and upper case letters</li>"
			+ "<li>Numbers</li>"
			+ "<li>Symbols: #$^+=!*()@%&~'\"\\-_/?<>;:[]{}</li>"
			+ "<li>Password must be different from the last 15 passwords.</li></ul>";

	protected UserDao userDao;
	protected CommonRequestArgsFactory craf;
	
	public DefaultPasswordValidator(UserDao userDao, CommonRequestArgsFactory craf) {
		this.userDao = Preconditions.checkNotNull(userDao);
		this.craf = Preconditions.checkNotNull(craf);
	}
	
	public PasswordPolicies getPwdPolicies() {
		PasswordPolicies pp = new PasswordPolicies();
		pp.setPwdRulesDescEn(pwdRulesPopoverMsgEn);
		pp.setPwdRulesDescFr(pwdRulesPopoverMsgFr);
		return pp;
	}

	@Override
	public void validatePassword(String id, String pwd) {
		if (isNullOrEmpty(id)) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("ID"));
		}
		if (isNullOrEmpty(pwd)) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Password"));
		}
		
		// check rules
		if (Strings.isNullOrEmpty(pwdRulesRegEx)) {
			logger.warn("Pwd rules regex not set!");
			throw ApplicationException.exception(AuthMessageID.CHANGE_PWD_FAILED.getMessage());
		}
		
		if (!pwd.matches(pwdRulesRegEx)) {
			logger.warn("New pwd does not match rules for user={}, regex=", id, pwdRulesRegEx);
			throw ApplicationException.exception(AuthMessageID.CHANGE_PWD_MIN_REQ.getMessage());
		}

		CommonRequestArgs cra = craf.create();
		
		// check new pwd has not been used in the N recent pwds
		List<String> recentPwds = userDao.listUserRecentPwd(id, recentPwdsCount, cra);
		if (recentPwds != null) {
			for (String recentPwd : recentPwds) {
				if (CommonChecksumFunction.SHA512.validateChecksum(pwd, recentPwd)) {
					logger.warn("New pwd has been used recently for user={}", id);
					throw ApplicationException.exception(AuthMessageID.CHANGE_PWD_RECENT_USED.getMessage(Integer.toString(recentPwds.size())));
				}
			}
		}
	}

	@Override
	public long getPwdExpiryDays(Date pwdDate) {
		if (pwdDate == null) {
			// do not expire
			return -1;
		}
		
		long age = (new Date().getTime() -pwdDate.getTime()) / MS_PER_DAY;
		long ed = pwdMaxAgeDays -age;
		
		// already expired (zero) or will expire in <ed> days
		return Math.max(0, ed);
	}

	public void setRecentPwdsCount(int recentPwdsCount) {
		this.recentPwdsCount = recentPwdsCount;
	}

	public void setPwdRulesRegex(String pwdRulesRegEx) {
		this.pwdRulesRegEx = pwdRulesRegEx;
	}

	public void setPwdRulesPopoverMsgFr(String pwdRulesPopoverMsgFr) {
		this.pwdRulesPopoverMsgFr = pwdRulesPopoverMsgFr;
	}

	public void setPwdRulesPopoverMsgEn(String pwdRulesPopoverMsgEn) {
		this.pwdRulesPopoverMsgEn = pwdRulesPopoverMsgEn;
	}

	public void setPwdMaxAgeDays(int pwdMaxAgeDays) {
		this.pwdMaxAgeDays = pwdMaxAgeDays;
	}

}
