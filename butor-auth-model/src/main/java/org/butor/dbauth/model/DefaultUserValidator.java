/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import static com.google.common.base.Strings.isNullOrEmpty;

import org.butor.auth.common.user.User;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;

public class DefaultUserValidator implements UserValidator {
	public static final String DEFAULT_EMAIL_REGEX = "^[\\w\\.-]+@[\\w\\.-]+\\.[\\w\\.-]+$";
	public static final String DEFAULT_ID_REGEX = "([\\w\\.-]+)|(^[\\w\\.-]+@[\\w\\.-]+\\.[\\w\\.-]+$)";

	private String emailRegex = DEFAULT_EMAIL_REGEX;
	private String idRegex = DEFAULT_ID_REGEX;
	
	@Override
	public void validateUser(User user) {
		if (user == null) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("User"));
		}
		if (isNullOrEmpty(user.getId())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("ID"));
		}
		if (isNullOrEmpty(user.getEmail())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Email"));
		}
		if (!user.getEmail().matches(emailRegex)) {
			ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("Email"));
		}
		if (isNullOrEmpty(user.getFirstName())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("First name"));
		}
		if (isNullOrEmpty(user.getLastName())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Last name"));
		}
		if (isNullOrEmpty(user.getDisplayName())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Display name"));
		}
		if (user.getFirmId() <= 0) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Firm"));
		}
		validateId(user);
	}
	protected void validateId(User user) {
		// id could be any alphanum word or an email address
		if (isNullOrEmpty(user.getId())) {
			ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("ID"));
		}
		//force id to lower case
		user.setId(user.getId().toLowerCase());

		if (!user.getId().matches(idRegex)) {
			ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("ID"));
		}
	}
	public void setEmailRegex(String emailRegex) {
		if (isNullOrEmpty(emailRegex)) {
			ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("emailRegex"));
		}
		this.emailRegex = emailRegex;
	}
	public void setIdRegex(String idRegex) {
		if (isNullOrEmpty(idRegex)) {
			ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("idRegex"));
		}
		this.idRegex = idRegex;
	}
}
