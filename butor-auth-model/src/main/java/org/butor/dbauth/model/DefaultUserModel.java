/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import static com.google.common.base.Strings.isNullOrEmpty;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.dao.AttrSetDao;
import org.butor.attrset.util.Attributes;
import org.butor.attrset.util.Attributes.AttributesBuilder;
import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.AuthModel;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupModel;
import org.butor.auth.common.password.PasswordPolicies;
import org.butor.auth.common.password.PasswordValidator;
import org.butor.auth.common.user.ListUserCriteria;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserModel;
import org.butor.auth.common.user.UserQuestions;
import org.butor.auth.common.user.UserServices;
import org.butor.auth.dao.UserDao;
import org.butor.checksum.CommonChecksumFunction;
import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.json.util.CommonRequestArgsFactory;
import org.butor.ldap.LdapUserModel;
import org.butor.mail.EmailTemplate;
import org.butor.mail.IMailer;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonDateFormat;
import org.butor.utils.CommonMessageID;
import org.butor.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

public class DefaultUserModel implements UserServices, UserModel, InitializingBean {
	protected UserDao userDao;
	private Logger logger = LoggerFactory.getLogger(getClass());
	protected GroupModel groupModel;
	protected AuthModel authModel;
	protected CommonRequestArgsFactory adminCraf;

	protected IMailer mailer;
	protected String fromRecipient;
	protected AttrSetDao attrSetDao;
	protected UserValidator userValidator = new DefaultUserValidator();
	protected PasswordValidator pwdValidator = null;
	
	protected String[] attrsToHideInList = new String[] {"apiKey"};

	@Override
	public void readUser(Context<User> ctx, String id, String func) {
		ResponseHandler<User> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		User user = readUser(id, func, cra);
		if (user == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(user);
	}

	@Override
	public void readUsers(Context<User> ctx, List<String> idl) {
		List<User> ul = readUsers(idl, ctx.getRequest());
		ResponseHandlerHelper.addList(ul, ctx.getResponseHandler());
	}

	@Override
	public void insertUser(Context<UserKey> ctx, User user) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserKey uk = insertUser(user, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.SERVICE_FAILURE.getMessage());
			return;
		}

		rh.addRow(uk);
	}

	@Override
	public void updateUser(Context<UserKey> ctx, User user) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		
		// cleanup working attributes
		user.removeAttribute("pwdExpiryDays");
		
		UserKey uk = updateUser(user, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	@Transactional
	@Override
	public void deleteUser(Context<Void> ctx, UserKey userKey) {
		CommonRequestArgs cra = ctx.getRequest();
		deleteUser(userKey, cra);
	}

	@Override
	public void listUser(Context<User> ctx, ListUserCriteria criteria,
			String func) {
		CommonRequestArgs cra = ctx.getRequest();
		List<User> list = listUser(criteria, func, cra);
		ResponseHandlerHelper.addList(list, ctx.getResponseHandler());
	}

	@Override
	public void getPwdPolicies(Context<PasswordPolicies> ctx) {
		ctx.getResponseHandler().addRow(pwdValidator.getPwdPolicies());
	}

	@Override
	public void readQuestions(Context<UserQuestions> ctx, String id) {
		ResponseHandler<UserQuestions> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserQuestions uq = readQuestions(id, cra);
		if (uq == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uq);
	}

	@Override
	public void updateQuestions(Context<UserKey> ctx, UserQuestions questions) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserKey uk = updateQuestions(questions, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	@Override
	public void updateState(Context<UserKey> ctx, User user) {
		ResponseHandler<UserKey> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		UserKey uk = updateState(user, cra);
		if (uk == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(uk);
	}

	@Override
	public void resetLogin(Context<String> ctx, String id, String domain,
			boolean resetAndSendLink) {
		resetLogin(ctx, id, domain, true, resetAndSendLink);
	}

	@Override
	public void resetLogin(Context<String> ctx, String id, String domain,
			boolean resetQuestions, boolean resetAndSendLink) {
		ResponseHandler<String> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();

		String url = resetLogin(id, domain, resetQuestions, resetAndSendLink,
				cra);
		rh.addRow(url);
	}

	// UserModel ==========================

	@Override
	public User readUser(String id, String func, CommonRequestArgs cra) {
		User u = userDao.readUser(id, func, cra);
		if (u != null) {
			u.setPwdEditable(true);
			u.setAttribute("pwdExpiryDays", -1);
			
			String pd = (String) u.getAttribute("pwdDate");
			if (pd != null) {
				try {
					Date date = CommonDateFormat.YYYYMMDD.parse(pd);
					u.setAttribute("pwdExpiryDays", pwdValidator.getPwdExpiryDays(date));
				} catch (ParseException e) {
					logger.warn("Failed to parse password date {}", pd);
				}
				
			}
		}
		return u;
	}
	@Override
	public List<User> readUsers(List<String> idl, CommonRequestArgs cra) {
		return userDao.readUsers(idl, cra);
	}

	@Override
	public UserKey insertUser(User user, CommonRequestArgs cra) {
		validateUser(user);
		if (isNullOrEmpty(user.getFullName())) {
			user.setFullName(user.getDisplayName());
		}

		if (!isNullOrEmpty(user.getNewPwd())) {
			if (isNullOrEmpty(user.getNewPwdConf())) {
				ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Confirm new password"));
			}
			if (!user.getNewPwd().equals(user.getNewPwdConf())) {
				ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("new passwords do not match"));
			}

			pwdValidator.validatePassword(user.getId(), user.getNewPwd());

			user.setPwd(CommonChecksumFunction.SHA512.generateChecksum(user.getNewPwd()));
			
			user.setAttribute("pwdDate", CommonDateFormat.YYYYMMDD.format(new Date()));
		}

		user.setNewPwd(null);
		user.setNewPwdConf(null);
		
		return userDao.insertUser(user, cra);
	}

	protected void validateUser(User user) {
		userValidator.validateUser(user);
	}

	protected String getUserLang(User user, CommonRequestArgs cra) {
		String lang = user.getLanguage();
		if (isNullOrEmpty(lang)) {
			// user lang is in his profile
			Attributes attrs = new AttributesBuilder().setType("user")
					.setId(user.getId()).setAttrSetDao(attrSetDao)
					.setCommonRequestArgs(cra).build();

			if (attrs != null) {
				lang = attrs.get("language", ".");
			}
		}
		return lang;
	}

	protected EmailTemplate getEmailTemplate(String type, String lang,
			CommonRequestArgs cra) {
		Attributes attrs = new AttributesBuilder().setType(type)
				.setId("email-template").setAttrSetDao(attrSetDao)
				.setCommonRequestArgs(cra).build();

		if (isNullOrEmpty(lang)) {
			lang = cra.getLang();
		}
		EmailTemplate det = null;
		EmailTemplate et = null;
		Collection<AttrSet> list = attrs.list();
		for (AttrSet as : list) {
			if (as.getK1().equalsIgnoreCase(cra.getDomain())
					&& as.getK2().equalsIgnoreCase(lang)) {

				et = new JsonHelper().deserialize(as.getValue(),
						EmailTemplate.class);
				break;
			}
			// any default ?
			if (as.getK1().equals("*") && as.getK2().equalsIgnoreCase(lang)) {

				det = new JsonHelper().deserialize(as.getValue(),
						EmailTemplate.class);
			}
		}

		if (et == null) {
			et = det;
			if (et == null) {
				et = new EmailTemplate();
				et.setFromRecipient(fromRecipient);
				if (lang.equals("fr")) {
					et.setSubject("Reinitialisation login portail");
					et.setMessage("Votre login au portail a été reinitialisé à votre demande.\n"
							+ "SVP cliquer sur le lien plus bas et suivre les instructions.\n\n"
							+ "{link}");
				} else {
					et.setSubject("Portal login reset");
					et.setMessage("Your login to the portal has been reset as you requested.\n"
							+ "Please click on the link bellow and follow the instructions.\n\n"
							+ "{link}");
				}
			}
		}

		return et;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setLdapUserModel(LdapUserModel ldapUserModel) {
		logger.warn("LdapUserModel is not used!");
	}

	public void setMailer(IMailer mailer) {
		this.mailer = mailer;
	}

	public void setFromRecipient(String fromRecipient) {
		this.fromRecipient = fromRecipient;
	}

	public void setAttrSetDao(AttrSetDao attrSetDao) {
		this.attrSetDao = attrSetDao;
	}

	@Override
	public List<User> listUser(ListUserCriteria criteria, String func,
			CommonRequestArgs cra) {
		List<User> list = userDao.listUser(criteria, func, cra);
		Iterator<User> it = list.iterator();
		while (it.hasNext()) {
			User u = it.next();
			u.setPwd(null);
			if (attrsToHideInList != null) {
				Map<String, Object> am = u.getAttributesMap();
				if (am != null) {
					for (String at : attrsToHideInList) {
						am.remove(at);
					}
				}
			}
		}
		return list;
	}

	@Override
	public UserKey updateUser(User user, CommonRequestArgs cra) {
		validateUser(user);
		if (isNullOrEmpty(user.getFullName())) {
			user.setFullName(user.getDisplayName());
		}

		if (!isNullOrEmpty(user.getNewPwd())) {
			if (isNullOrEmpty(user.getNewPwdConf())) {
				ApplicationException.exception(CommonMessageID.MISSING_ARG.getMessage("Confirm new password"));
			}
			if (!user.getNewPwd().equals(user.getNewPwdConf())) {
				ApplicationException.exception(CommonMessageID.INVALID_ARG.getMessage("new passwords do not match"));
			}

			pwdValidator.validatePassword(user.getId(), user.getNewPwd());

			user.setPwd(CommonChecksumFunction.SHA512.generateChecksum(user.getNewPwd()));
			
			user.setAttribute("pwdDate", CommonDateFormat.YYYYMMDD.format(new Date()));
		}

		user.setNewPwd(null);
		user.setNewPwdConf(null);
		
		return userDao.updateUser(user, cra);
	}

	@Override
	public void deleteUser(UserKey userKey, CommonRequestArgs cra) {
		User user = userDao.readUser(userKey.getId(), null, cra);
		if (user == null) {
			ApplicationException.exception(AuthMessageID.USER_NOT_FOUND
					.getMessage());
		}
		if (user.isActive()) {
			ApplicationException
					.exception(AuthMessageID.USER_SHOULD_BE_INACTIVE_TO_BE_DELETED
							.getMessage());
		}

		// remove user from groups,
		// list with admin user to be sure to find all applicable data
		CommonRequestArgs adminCra = adminCraf.create();
		List<Group> gl = groupModel.listGroup(userKey.getId(), null, adminCra);
		for (Group g : gl) {
			Group grp = groupModel.readGroup(g.getId(), cra);
			boolean changed = false;
			if (grp != null && grp.getItems() != null) {
				Iterator<GroupItem> it = grp.getItems().iterator();
				while (it.hasNext()) {
					GroupItem gi = it.next();
					if (gi.getMember().equalsIgnoreCase(userKey.getId())) {
						it.remove();
						changed = true;
						break;
					}
				}
			}
			if (changed) {
				// update with request user if he has access
				groupModel.updateGroup(grp, cra);
			}
		}

		// remove user direct authorisations
		// list with admin user to be sure to find all applicable data
		ListAuthCriteria lac = new ListAuthCriteria();
		lac.setWhoTypes(Arrays.asList("user"));
		lac.setWhos(Arrays.asList(userKey.getId()));
		List<Auth> al = authModel.listAuth(lac, adminCra);
		for (Auth a : al) {
			// delete with request user if he has access
			authModel.deleteAuth(new AuthKey(a.getAuthId(), a.getRevNo()), cra);
		}

		// remove user
		userDao.deleteUser(userKey, cra);
	}

	@Override
	public UserQuestions readQuestions(String id, CommonRequestArgs cra) {
		return userDao.readQuestions(id, cra);
	}

	@Override
	public UserKey updateQuestions(UserQuestions questions,
			CommonRequestArgs cra) {
		return userDao.updateQuestions(questions, cra);
	}

	@Override
	public UserKey updateState(User user, CommonRequestArgs cra) {
		return userDao.updateState(user, cra);
	}

	@Override
	public String resetLogin(String id, String domain,
			boolean resetAndSendLink, CommonRequestArgs cra) {
		return resetLogin(id, domain, true, resetAndSendLink, cra);
	}

	@Override
	public String resetLogin(String id, String domain, boolean resetQuestions,
			boolean resetAndSendLink, CommonRequestArgs cra) {

		if (mailer == null && resetAndSendLink) {
			logger.warn("Mailer in not set!");
			throw ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}
		if (StringUtil.isEmpty(id)) {
			logger.warn("Missing credential id arg");
			throw ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}

		User user = readUser(id, SecurityConstants.SEC_FUNC_USERS, cra);
		if (user == null) {
			logger.warn("Unknown user {}", id);
			ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}
		if (StringUtil.isEmpty(user.getEmail())) {
			logger.warn(String.format("No email defined for id %s", id));
			ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}

		String tokSeed = UUID.randomUUID().toString();

		// gen token for reset
		String resetToken = CommonChecksumFunction.SHA256
				.generateChecksumWithTTL(tokSeed, 2, TimeUnit.HOURS);
		user.setPwd(resetToken);
		user.setResetInProgress(true);
		user.setMissedLogin(0);

		if (resetQuestions) {
			UserQuestions uq = userDao.readQuestions(user.getId(), cra);
			if (uq != null) {
				uq.setQ1(null);
				uq.setQ2(null);
				uq.setQ3(null);
				uq.setLastQ(0);
				UserKey uk = userDao.updateQuestions(uq, cra);
				user.setRevNo(uk.getRevNo());
			}
		}

		UserKey uk = userDao.updateUser(user, cra);
		if (uk.getId() == null) {
			logger.warn(String.format(
					"Failed to save new password for user=%s", id));
			ApplicationException.exception(AuthMessageID.RESET_PWD_FAILED
					.getMessage());
		}

		String url = domain;
		if (isNullOrEmpty(url)) {
			url = cra.getDomain();
		}

		url += "/reset?t=" + resetToken + "&id=" + user.getId();

		String lang = getUserLang(user, cra);

		EmailTemplate et = getEmailTemplate("reset-login", lang, cra);
		String msg = et.getMessage();
		msg = msg.replace("{username}", user.getDisplayName());
		msg = msg.replace("{link}", url);
		String fromRecipient = et.getFromRecipient();
		if (isNullOrEmpty(fromRecipient)) {
			fromRecipient = this.fromRecipient;
		}
		logger.info(String.format(
				"Sending reset link via email for user=%s\n%s", id, msg));

		if (resetAndSendLink) {
			mailer.sendMail(user.getEmail(), et.getSubject(), msg,
					fromRecipient);
		} else {
			return url;
		}
		return null;
	}

	public void setUserValidator(UserValidator userValidator) {
		this.userValidator = userValidator;
	}

	public void setGroupModel(GroupModel groupModel) {
		this.groupModel = groupModel;
	}

	public void setAuthModel(AuthModel authModel) {
		this.authModel = authModel;
	}

	public void setAdminCraf(CommonRequestArgsFactory adminCraf) {
		this.adminCraf = adminCraf;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Preconditions.checkNotNull(userDao, "userDao not set");
		Preconditions.checkNotNull(groupModel, "groupModel not set");
		Preconditions.checkNotNull(authModel, "authModel not set");
		Preconditions.checkNotNull(adminCraf, "adminCraf not set");
		Preconditions.checkNotNull(mailer, "mailer not set");
		Preconditions.checkNotNull(attrSetDao, "attrSetDao not set");
		Preconditions.checkNotNull(userValidator, "userValidator not set");
		
		if (pwdValidator == null) {
			pwdValidator = new DefaultPasswordValidator(userDao, adminCraf);
		}
	}

	public void setAttrsToHideInList(String[] attrsToHideInList) {
		this.attrsToHideInList = attrsToHideInList;
	}

	public void setPwdValidator(PasswordValidator pwdValidator) {
		this.pwdValidator = pwdValidator;
	}
}
