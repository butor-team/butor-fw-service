/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.dbauth.model;

import java.util.List;

import org.butor.auth.common.AuthMessageID;
import org.butor.auth.common.SecurityConstants;
import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.firm.FirmKey;
import org.butor.auth.common.firm.FirmServices;
import org.butor.auth.common.firm.FirmWithAccessMode;
import org.butor.auth.common.firm.ListFirmCriteria;
import org.butor.auth.common.user.ListUserCriteria;
import org.butor.auth.common.user.User;
import org.butor.auth.dao.FirmDao;
import org.butor.auth.dao.UserDao;
import org.butor.json.CommonRequestArgs;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.utils.AccessMode;
import org.butor.utils.ApplicationException;
import org.butor.utils.CommonMessageID;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;

public class FirmModel implements FirmServices {
	private FirmDao firmDao;
	private UserDao userDao;
	
	@Override
	public void listFirm(Context<FirmWithAccessMode> ctx, ListFirmCriteria crit) {
		ResponseHandler<FirmWithAccessMode> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		List<FirmWithAccessMode> list = firmDao.listFirm(crit, cra);
		ResponseHandlerHelper.addList(list, rh);
	}

	@Override
	public void readFirm(Context<Firm> ctx, long firmId, String sys, String func, AccessMode mode) {
		ResponseHandler<Firm> rh = ctx.getResponseHandler();
		CommonRequestArgs cra = ctx.getRequest();
		Firm firm = firmDao.readFirm(firmId, sys, func, mode, cra);
		if (firm == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(firm);
	}

	@Override
	@Transactional
	public void insertFirm(Context<FirmKey> ctx, Firm firm) {
		ResponseHandler<FirmKey> rh = ctx.getResponseHandler();
		if (Strings.isNullOrEmpty(firm.getFirmName())) {
			rh.addMessage(CommonMessageID.MISSING_ARG.getMessage("Name"));
			return;
		}
		CommonRequestArgs cra = ctx.getRequest();
		FirmKey ck = firmDao.insertFirm(firm, cra);
		if (ck == null) {
			rh.addMessage(CommonMessageID.SERVICE_FAILURE.getMessage());
			return;
		}
		rh.addRow(ck);
	}

	@Override
	@Transactional
	public void updateFirm(Context<FirmKey> ctx, Firm firm) {
		ResponseHandler<FirmKey> rh = ctx.getResponseHandler();
		if (Strings.isNullOrEmpty(firm.getFirmName())) {
			rh.addMessage(CommonMessageID.MISSING_ARG.getMessage("Name"));
			return;
		}
		CommonRequestArgs cra = ctx.getRequest();
		FirmKey ck = firmDao.updateFirm(firm, cra);
		if (ck == null) {
			rh.addMessage(CommonMessageID.NOT_FOUND.getMessage());
			return;
		}
		rh.addRow(ck);
	}

	@Override
	@Transactional
	public void deleteFirm(Context<Firm> ctx, FirmKey firmKey) {
		CommonRequestArgs cra = ctx.getRequest();
		Firm firm = firmDao.readFirm(firmKey.getFirmId(), SecurityConstants.SYSTEM_ID, SecurityConstants.SEC_FUNC_FIRMS, AccessMode.READ, cra);
		if (firm == null) {
			ApplicationException.exception(AuthMessageID.FIRM_NOT_FOUND.getMessage());
		}
		if (firm.isActive()) {
			ApplicationException.exception(AuthMessageID.FIRM_SHOULD_BE_INACTIVE_TO_BE_DELETED.getMessage());
		}
		ListUserCriteria crit = new ListUserCriteria();
		crit.setFirmId(firm.getFirmId());
		List<User> users = userDao.listUser(crit, null, cra);
		if (users.size()>0) {
			ApplicationException.exception(AuthMessageID.FIRM_SHOULD_NOT_HAVE_USERS_TO_BE_DELETED.getMessage());
		}
		firmDao.deleteFirm(firmKey, cra);
	}

	public void setFirmDao(FirmDao firmDao) {
		this.firmDao = firmDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
