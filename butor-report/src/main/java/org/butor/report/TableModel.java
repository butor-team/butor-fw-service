/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.report;

import org.apache.poi.ss.usermodel.CellStyle;

public interface TableModel {
	int getColumnsCount();
	String getHeader(int index, String lang);
	CellStyle getColumnStyle(int index, String lang);
	Object getValue(Object rec, int index, String lang);
	int getColumnWidth(int index, String lang);
	String getColumnName(int index);
	int getColumnIndex(String name);
	void preProcessingRow(Object row);
	boolean hasTotalRow();
	int[] getTotalColumns();
}
