/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.report;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class XLSReportUtils {

	public final CellStyle csBold;
	public final CellStyle csOverline;
	public final CellStyle csUnderline;
	public final CellStyle csTableNumber;
	public final CellStyle csTableFXRate;
	public final CellStyle csTableCurrency;
	public final CellStyle csTablePrice;
	public final CellStyle csTableHeader;
	public final CellStyle csTableString;
	public final CellStyle csTableStringCentered;
	public final CellStyle csTableStringRight;
	public final CellStyle csTitle;
	public final CellStyle csAlignRight;
	public final CellStyle csPlain;

	public XLSReportUtils(Workbook wb) {
		Font f = wb.createFont();
		f.setFontHeightInPoints((short) 10);

		Font bold = wb.createFont();
		bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
		bold.setFontHeightInPoints((short) 10);

		csOverline = wb.createCellStyle();
		csOverline.setBorderTop(CellStyle.BORDER_THIN);
		csOverline.setTopBorderColor(IndexedColors.BLACK.getIndex());

		csUnderline = wb.createCellStyle();
		csUnderline.setBorderBottom(CellStyle.BORDER_THIN);
		csUnderline.setTopBorderColor(IndexedColors.BLACK.getIndex());

		csBold = wb.createCellStyle();
		csBold.setFont(bold);

		csTableHeader = wb.createCellStyle();
		csTableHeader.setBorderBottom(CellStyle.BORDER_THIN);
		csTableHeader.setBorderTop(CellStyle.BORDER_THIN);
		csTableHeader.setBorderRight(CellStyle.BORDER_THIN);
		csTableHeader.setBorderLeft(CellStyle.BORDER_THIN);
		csTableHeader.setFillBackgroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		csTableHeader.setFont(bold);

		DataFormat format = wb.createDataFormat();
		csTableCurrency = wb.createCellStyle();
		csTableCurrency.setDataFormat(format.getFormat("#,##0.00"));
		csTableCurrency.setBorderBottom(CellStyle.BORDER_THIN);
		csTableCurrency.setBorderTop(CellStyle.BORDER_THIN);
		csTableCurrency.setBorderRight(CellStyle.BORDER_THIN);
		csTableCurrency.setBorderLeft(CellStyle.BORDER_THIN);

		csTablePrice = wb.createCellStyle();
		csTablePrice.setDataFormat(format.getFormat("#,##0.00000"));
		csTablePrice.setBorderBottom(CellStyle.BORDER_THIN);
		csTablePrice.setBorderTop(CellStyle.BORDER_THIN);
		csTablePrice.setBorderRight(CellStyle.BORDER_THIN);
		csTablePrice.setBorderLeft(CellStyle.BORDER_THIN);

		csTableNumber = wb.createCellStyle();
		csTableNumber.setDataFormat(format.getFormat("#,###"));
		csTableNumber.setBorderBottom(CellStyle.BORDER_THIN);
		csTableNumber.setBorderTop(CellStyle.BORDER_THIN);
		csTableNumber.setBorderRight(CellStyle.BORDER_THIN);
		csTableNumber.setBorderLeft(CellStyle.BORDER_THIN);

		csTableFXRate = wb.createCellStyle();
		csTableFXRate.setDataFormat(format.getFormat("#,##0.0000"));
		csTableFXRate.setBorderBottom(CellStyle.BORDER_THIN);
		csTableFXRate.setBorderTop(CellStyle.BORDER_THIN);
		csTableFXRate.setBorderRight(CellStyle.BORDER_THIN);
		csTableFXRate.setBorderLeft(CellStyle.BORDER_THIN);

		csTableString = wb.createCellStyle();
		csTableString.setWrapText(true);
		//csTableString.setAlignment(CellStyle.ALIGN_CENTER);
		csTableString.setBorderBottom(CellStyle.BORDER_THIN);
		csTableString.setBorderTop(CellStyle.BORDER_THIN);
		csTableString.setBorderRight(CellStyle.BORDER_THIN);
		csTableString.setBorderLeft(CellStyle.BORDER_THIN);

		csTableStringCentered = wb.createCellStyle();
		csTableStringCentered.setWrapText(true);
		csTableStringCentered.setAlignment(CellStyle.ALIGN_CENTER);
		csTableStringCentered.setBorderBottom(CellStyle.BORDER_THIN);
		csTableStringCentered.setBorderTop(CellStyle.BORDER_THIN);
		csTableStringCentered.setBorderRight(CellStyle.BORDER_THIN);
		csTableStringCentered.setBorderLeft(CellStyle.BORDER_THIN);

		csTableStringRight = wb.createCellStyle();
		csTableStringRight.setWrapText(true);
		csTableStringRight.setAlignment(CellStyle.ALIGN_RIGHT);
		csTableStringRight.setBorderBottom(CellStyle.BORDER_THIN);
		csTableStringRight.setBorderTop(CellStyle.BORDER_THIN);
		csTableStringRight.setBorderRight(CellStyle.BORDER_THIN);
		csTableStringRight.setBorderLeft(CellStyle.BORDER_THIN);

		csAlignRight = wb.createCellStyle();
		csAlignRight.setWrapText(true);
		csAlignRight.setAlignment(CellStyle.ALIGN_RIGHT);

		csPlain = wb.createCellStyle();

		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 24);
		csTitle = wb.createCellStyle();
		csTitle.setFont(font);
	}
	public int printTableHeader(Sheet sheet, int rowIndex, 
			int cellOffset, TableModel tm, String lang) {
		Row row = sheet.getRow(rowIndex);
		if (row == null) {
			row = sheet.createRow(rowIndex);
		}
		rowIndex++;
		int cellIndex = cellOffset;
		int cc = tm.getColumnsCount();
		for (int c=0; c<cc; c+=1) {
			String label = tm.getHeader(c, lang);
			Cell cell = row.getCell(cellIndex);
			if (cell == null) {
				cell = row.createCell(cellIndex);
			}
			cellIndex++;
			cell.setCellStyle(csTableHeader);
			cell.setCellValue(label);

			int width = tm.getColumnWidth(c, lang);
			if (width == -1) {
				sheet.autoSizeColumn(c, true);
			} else {
				sheet.setColumnWidth(c, width);
			}
		}
		return rowIndex;
	}
	public int printTable(Workbook wb, Sheet sheet, int rowIndex, int cellOffset, 
			TableModel tm, List data, String lang) {
		
		 return printTable(wb, sheet, rowIndex, cellOffset, tm, data, lang, null, null);
	}
	
	public int printTable(Workbook wb, Sheet sheet, int rowIndex, int cellOffset, 
			TableModel tm, List data, String lang, Integer maxNbDisplayedRows, String dataTruncatedMsg) {

		rowIndex = printTableHeader(sheet, rowIndex, cellOffset, tm, lang);

		int firstRow = rowIndex +1;
		
		int nbItems= data.size();		
		boolean isDataTruncated= false;
		
		if (maxNbDisplayedRows != null && data.size() > maxNbDisplayedRows) {
			nbItems= maxNbDisplayedRows;
			isDataTruncated= true;
		}
		
		for (int i=0;i<nbItems; i+=1) {
			Object rec = data.get(i);
			tm.preProcessingRow(rec);
			Row row = sheet.getRow(rowIndex);
			if (row == null) {
				row = sheet.createRow(rowIndex);
			}
			rowIndex++;

			int cellIndex = cellOffset;
			int cc = tm.getColumnsCount();
			for (int c=0; c<cc; c+=1) {
				Object value = tm.getValue(rec, c, lang);
				CellStyle cs = tm.getColumnStyle(c, lang);

				Cell cell = row.getCell(cellIndex);
				if (cell == null) {
					cell = row.createCell(cellIndex);
				}
				cellIndex++;
				if (null != cs) {
					cell.setCellStyle(cs);
				}

				if (value == null) {
					cell.setCellValue("");
				} else if (value instanceof String) {
					cell.setCellValue((String) value);
				} else if (value instanceof BigDecimal) {
					cell.setCellValue(((BigDecimal)value).doubleValue());
				} else if (value instanceof Boolean) {
					cell.setCellValue((Boolean)value);
				} else if (value instanceof Date) {
					cell.setCellValue((Date)value);
				} else if (value instanceof Calendar) {
					cell.setCellValue((Calendar)value);
				} else {
					cell.setCellValue(value.toString());
				}
			}
		}
		if (tm.hasTotalRow()) {
			Row row = sheet.getRow(rowIndex);
			if (row == null) {
				row = sheet.createRow(rowIndex);
			}
			Cell cell = row.createCell(cellOffset);
			cell.setCellStyle(csTableString);
			cell.setCellValue("TOTAL");

			int[] totalCols = tm.getTotalColumns();
			for (int c : totalCols) {
				String l = new String(new byte[]{(byte)(65+c)});
				cell = row.createCell(c +cellOffset);
				cell.setCellStyle(csTableCurrency);
				cell.setCellFormula("sum(" +l +firstRow +":" +l +rowIndex +")");
			}
		}
		
		if (isDataTruncated) {
			
			Row row = sheet.getRow(++rowIndex);
			
			if (row == null) {
				row = sheet.createRow(rowIndex);
			}

			Cell cell = row.createCell(2 +cellOffset);
			
			cell.setCellStyle(csBold);
			cell.setCellValue(dataTruncatedMsg);						
		}

		int cc = tm.getColumnsCount();
		for (int c=0; c<cc; c+=1) {
			// Set Column Widths
			int width = tm.getColumnWidth(c, lang);
			if (width > -1) {
				sheet.setColumnWidth(cellOffset +c, width);
			}
		}

		return rowIndex;
	}
}
