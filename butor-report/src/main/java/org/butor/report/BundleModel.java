/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.report;

import java.util.HashMap;
import java.util.Map;

import org.butor.json.JsonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BundleModel {
	private Logger logger = LoggerFactory.getLogger(getClass());

	private Map<String, Map<String, String>> bundle;
	
	public BundleModel(String bundleJson) {
		this.bundle = new JsonHelper().deserialize(bundleJson, Map.class);
	}

	public String get(String key, String lang) {
		Map<String, String> entry = bundle.get(key);
		if (null==entry) {
			return key;
		}
		String val = entry.get(lang);
		return val == null ? key : val;
	}
	public synchronized void add(String key, String lang, String value) {
		Map<String, String> entry = bundle.get(key);
		if (entry == null) {
			entry = new HashMap<String, String>();
			bundle.put(key, entry);
		}
		entry.put(lang, value);
	}
}
