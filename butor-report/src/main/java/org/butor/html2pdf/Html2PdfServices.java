/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.html2pdf;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.butor.json.service.BinResponseHandler;
import org.butor.json.service.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.client.util.IOUtils;

public class Html2PdfServices {
	private Logger logger = LoggerFactory.getLogger(getClass());

	protected String html2pdfBase;

	public Html2PdfServices(String html2pdfBase) {
		this.html2pdfBase = checkNotNull(html2pdfBase);
	}

	public void html2pdf(final Context<?> ctx, Map<String, String> args) {
		BinResponseHandler brh = (BinResponseHandler) ctx.getResponseHandler();

		Map<String, String> headers = new HashMap<String, String>();
		Html2Pdf h2p = new Html2Pdf(html2pdfBase);
		h2p.html2pdf(args, new Html2PdfHandler() {
			@Override
			public void handleFile(File file) {
				try {
					if (file != null) {
						headers.put("Content-disposition", "attachment; filename=\"" + args.get("name") +"\"");
						brh.setContentType("application/pdf", headers);
			
						FileInputStream is = new FileInputStream(file);
						IOUtils.copy(is, brh.getOutputStream(), true);
			
					} else {
						brh.setContentType("text/plain", headers);
						brh.getOutputStream().write("Oups! Failed to export pdf!".getBytes());
					}
				} catch (IOException e) {
					logger.warn("Failed! {}", args, e);
				}
			}
		});
	}
}
