/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.audit;

import java.util.Date;

import org.butor.utils.BeanWithAttributes;

public class ServiceCall extends BeanWithAttributes{
	
	private String user;
	private String namespace;
	private String service;
	private String payload;
	private boolean success;
	private long duration;
	private Date serviceCallTimestamp;
	private String reqId;
	private String sessionId;
	
	public String getUser() {
		return user;
	}
	public String getPayload() {
		return payload;
	}
	public boolean isSuccess() {
		return success;
	}
	public long getDuration() {
		return duration;
	}
	public Date getServiceCallTimestamp() {
		return serviceCallTimestamp;
	}
	public String getReqId() {
		return reqId;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public void setPayload(String payload) {
		this.payload = payload;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public void setDuration(long duration) {
		this.duration = duration;
	}
	public void setServiceCallTimestamp(Date serviceCallTimestamp) {
		this.serviceCallTimestamp = serviceCallTimestamp;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
}