/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common;

import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.auth.ListUserAuthFuncCriteria;
import org.butor.json.service.Context;
import org.butor.utils.AccessMode;

public interface AuthServices {
	void hasAccess(Context ctx, String system, String func, AccessMode am);
	void listAuthFunc(Context ctx);
	void listAuthSys(Context ctx, String system, String function);
	void listAuthData(Context ctx, ListAuthDataCriteria criteria);
	void listAuth(Context ctx, ListAuthCriteria criteria);
	void listUserAuthFunc(Context ctx, ListUserAuthFuncCriteria criteria);
	void readAuth(Context ctx, long authId);
	void updateAuth(Context ctx, Auth auth);
	void deleteAuth(Context ctx, AuthKey ak);
	void createAuth(Context ctx, Auth auth);
}
