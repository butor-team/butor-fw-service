/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.role;

public class RoleItemKey {
	private String roleId;
	private String func;
	private String sys;
	private int revNo;
	public RoleItemKey() {
	}
	public RoleItemKey(String roleId, String func, String sys) {
		this(roleId, func, sys, 0);
	}
	public RoleItemKey(String roleId, String func, String sys, int revNo) {
		super();
		this.roleId = roleId;
		this.func = func;
		this.sys = sys;
		this.revNo = revNo;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public int getRevNo() {
		return revNo;
	}
	public void setRevNo(int revNo) {
		this.revNo = revNo;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((func == null) ? 0 : func.hashCode());
		result = prime * result + revNo;
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result + ((sys == null) ? 0 : sys.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleItemKey other = (RoleItemKey) obj;
		if (func == null) {
			if (other.func != null)
				return false;
		} else if (!func.equals(other.func))
			return false;
		if (revNo != other.revNo)
			return false;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		if (sys == null) {
			if (other.sys != null)
				return false;
		} else if (!sys.equals(other.sys))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "GroupItemKey [roleId=" + roleId + ", func=" + func + ", sys=" + sys + ", revNo=" + revNo + "]";
	}
	public String getFunc() {
		return func;
	}
	public void setFunc(String func) {
		this.func = func;
	}
	public String getSys() {
		return sys;
	}
	public void setSys(String sys) {
		this.sys = sys;
	}
}
