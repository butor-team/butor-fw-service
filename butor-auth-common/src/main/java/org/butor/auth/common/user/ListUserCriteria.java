/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common.user;

public class ListUserCriteria {
	private String email;
	private String displayName;
	private long firmId;
	private Boolean locked;
	private Boolean resetInProgress;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public long getFirmId() {
		return firmId;
	}
	public void setFirmId(long firmId) {
		this.firmId = firmId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((displayName == null) ? 0 : displayName.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (int) (firmId ^ (firmId >>> 32));
		result = prime * result + ((locked == null) ? 0 : locked.hashCode());
		result = prime * result + ((resetInProgress == null) ? 0 : resetInProgress.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ListUserCriteria other = (ListUserCriteria) obj;
		if (displayName == null) {
			if (other.displayName != null)
				return false;
		} else if (!displayName.equals(other.displayName))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firmId != other.firmId)
			return false;
		if (locked == null) {
			if (other.locked != null)
				return false;
		} else if (!locked.equals(other.locked))
			return false;
		if (resetInProgress == null) {
			if (other.resetInProgress != null)
				return false;
		} else if (!resetInProgress.equals(other.resetInProgress))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ListUserCriteria [email=" + email + ", displayName=" + displayName + ", firmId=" + firmId + ", locked="
				+ locked + ", resetInProgress=" + resetInProgress + "]";
	}
	public Boolean getLocked() {
		return locked;
	}
	public void setLocked(Boolean locked) {
		this.locked = locked;
	}
	public Boolean getResetInProgress() {
		return resetInProgress;
	}
	public void setResetInProgress(Boolean resetInProgress) {
		this.resetInProgress = resetInProgress;
	}
}
