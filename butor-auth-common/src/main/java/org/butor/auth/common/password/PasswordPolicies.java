/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package org.butor.auth.common.password;

/**
 * @author asawan
 *
 */
public class PasswordPolicies {
	private String pwdRulesDescFr;
	private String pwdRulesDescEn;
	public String getPwdRulesDescFr() {
		return pwdRulesDescFr;
	}
	public void setPwdRulesDescFr(String pwdRulesDescFr) {
		this.pwdRulesDescFr = pwdRulesDescFr;
	}
	public String getPwdRulesDescEn() {
		return pwdRulesDescEn;
	}
	public void setPwdRulesDescEn(String pwdRulesDescEn) {
		this.pwdRulesDescEn = pwdRulesDescEn;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pwdRulesDescEn == null) ? 0 : pwdRulesDescEn.hashCode());
		result = prime * result + ((pwdRulesDescFr == null) ? 0 : pwdRulesDescFr.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PasswordPolicies other = (PasswordPolicies) obj;
		if (pwdRulesDescEn == null) {
			if (other.pwdRulesDescEn != null)
				return false;
		} else if (!pwdRulesDescEn.equals(other.pwdRulesDescEn))
			return false;
		if (pwdRulesDescFr == null) {
			if (other.pwdRulesDescFr != null)
				return false;
		} else if (!pwdRulesDescFr.equals(other.pwdRulesDescFr))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PasswordPolicies [pwdRulesDescFr=" + pwdRulesDescFr + ", pwdRulesDescEn=" + pwdRulesDescEn + "]";
	}
}
