/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common;

import java.util.List;
import java.util.Map;

import org.butor.auth.common.ListAuthDataCriteria;
import org.butor.auth.common.auth.Auth;
import org.butor.auth.common.auth.AuthKey;
import org.butor.auth.common.auth.ListAuthCriteria;
import org.butor.auth.common.auth.SecData;
import org.butor.auth.common.func.Func;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.AccessMode;

public interface AuthModel {
	boolean hasAccess(String system, String func, AccessMode am, CommonRequestArgs cra);
	List<Func> listAuthFunc(CommonRequestArgs cra);
	List<AuthData> listAuthData(ListAuthDataCriteria criteria, CommonRequestArgs cra);
	Map<String, List<String>> mapAuthData(String system, String func, AccessMode am, 
			CommonRequestArgs cra, String... dataTypes);
	Auth readAuth(long authId, CommonRequestArgs cra);
	AuthKey createAuth(Auth auth, CommonRequestArgs cra);
	void deleteAuth(AuthKey ak, CommonRequestArgs cra);
	AuthKey updateAuth(Auth auth, CommonRequestArgs cra);
	List<Auth> listAuth(ListAuthCriteria criteria, CommonRequestArgs cra);
	List<SecData> listData(SecData criteria, CommonRequestArgs cra);
	void updateData(long dataId, List<SecData> data, CommonRequestArgs cra);
}
