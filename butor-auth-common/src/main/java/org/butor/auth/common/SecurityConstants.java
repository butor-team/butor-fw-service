/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.common;

public final class SecurityConstants {
	public static final String SYSTEM_ID = "sec";
	public static final String SEC_FUNC_USERS = "users";
	public static final String SEC_FUNC_FIRMS = "firms";
	public static final String SEC_FUNC_FUNCS = "funcs";
	public static final String SEC_FUNC_ROLES = "roles";
	public static final String SEC_FUNC_GROUPS = "groups";
	public static final String SEC_FUNC_AUTHS = "auths";
	public static final String SEC_FUNC_AUDIT = "audit";
	public static final String SEC_FUNC_AUDIT_SERVICECALL = "auditServiceCall";

	public static final String SEC_DATA_TYPE_USER = "user";
	public static final String SEC_DATA_TYPE_FIRM = "firm";
	
}
