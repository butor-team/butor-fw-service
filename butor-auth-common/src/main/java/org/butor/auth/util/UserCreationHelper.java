/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.auth.util;

import java.util.List;

import org.butor.auth.common.firm.FirmServices;
import org.butor.auth.common.firm.FirmWithAccessMode;
import org.butor.auth.common.firm.ListFirmCriteria;
import org.butor.auth.common.group.Group;
import org.butor.auth.common.group.GroupItem;
import org.butor.auth.common.group.GroupServices;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserServices;
import org.butor.json.service.Context;
import org.butor.json.service.DefaultResponseHandler;
import org.butor.json.util.ContextBuilder;
import org.butor.utils.ApplicationException;
import org.butor.utils.Message;
import org.butor.utils.Message.MessageType;
import org.springframework.beans.factory.InitializingBean;

import com.google.common.base.Preconditions;

/**
 * Tool that wraps services calls to help creating users
 * with an easy to use interface
 *  
 * @author tbussier
 *
 */
public class UserCreationHelper implements InitializingBean {

	private String adminUser;
	private String hostname;

	private UserServices userServices;
	private FirmServices firmServices;
	private GroupServices groupServices;

	/**
	 * 
	 * Create a user
	 * Firm name is mandatory and the firm should exist in the system.
	 * 
	 * @param user
	 * @param firmName
	 * @return true if the user was created
	 */
	public boolean createUser(User user, String firmName) {
		DefaultResponseHandler<UserKey> responseHandler  = createResponseHandler(UserKey.class);
		Context<UserKey> context = createContext(responseHandler);
		Long firmId = getFirmId(firmName);
		if (firmId == null) {
			return false;
		}
		user.setFirmId(firmId);
		user.setFirmName(firmName);
		userServices.insertUser(context, user);
		validateMessages(responseHandler.getMessages());
		return true;
	}
	
	

	/**
	 * Check if the userId exists
	 * @param userId
	 * @return true if the user exists
	 */
	public boolean userExists(String userId) {
		DefaultResponseHandler<User> responseHandler  = createResponseHandler(User.class);
		Context<User> context = createContext(responseHandler);
		userServices.readUser(context, userId, null);
		if (!responseHandler.getRows().isEmpty()) {
			return true;
		} 
		validateMessages(responseHandler.getMessages());
		return false;
	}
	
	public void updateUser(User user) {
		DefaultResponseHandler<UserKey> responseHandler  = createResponseHandler(UserKey.class);
		Context<UserKey> context = createContext(responseHandler);
		userServices.updateUser(context, user);
	}
	
	/**
	 * Get a user
	 * @param userId
	 * @return user if the user exists
	 */
	public User getUser(String userId) {
		DefaultResponseHandler<User> responseHandler  = createResponseHandler(User.class);
		Context<User> context = createContext(responseHandler);
		userServices.readUser(context, userId, null);
		if (!responseHandler.getRows().isEmpty()) {
			return responseHandler.getRow();
		} 
		return null;
	}


	/**
	 * Adds a userId to a groupId
	 * Return false if the user is already a member of the group
	 *    
	 * @param userId
	 * @param groupId
	 * @return true if the group have been modified
	 */
	public boolean addUserToGroup(String userId, String groupId) {
		Group g = getGroup(groupId);
		for (GroupItem gi  : g.getItems()) {
			if (userId.equals(gi.getUserId())) {
				return false;
			}
		}
		GroupItem gi = new GroupItem();
		gi.setGroupId(groupId);
		gi.setUserId(adminUser);
		gi.setMember(userId);
		g.getItems().add(gi);
		DefaultResponseHandler<Group> responseHandler  = createResponseHandler(Group.class);
		Context<Group> context = createContext(responseHandler);
		groupServices.updateGroup(context, g);
		validateMessages(responseHandler.getMessages());
		return true;
	}
	/**
	 * Validates messages.
	 * 
	 * This default behavior throws an exception when one of the messages is of type ERROR
	 * 
	 * @param messages
	 */
	protected void validateMessages(List<Message> messages) {
		for (Message m : messages) {
			if (MessageType.ERROR.equals(m.getType())) {
				throw ApplicationException.exception(m);
			}
		}
	}
	
	private Long getFirmId(String firmName) {
		DefaultResponseHandler<FirmWithAccessMode> responseHandler = createResponseHandler(FirmWithAccessMode.class);
		Context<FirmWithAccessMode> context = createContext(responseHandler);
		ListFirmCriteria criteria = new ListFirmCriteria();
		criteria.setFirmName(firmName);
		firmServices.listFirm(context, criteria);
		List<Message> messages = responseHandler.getMessages();
		if (!responseHandler.getRows().isEmpty()) {
			return responseHandler.getRow().getFirmId();
		}
		validateMessages(messages);
		return null;
	}
	
	private Group getGroup(String groupId) {
		DefaultResponseHandler<Group> responseHandler  =  createResponseHandler(Group.class);
		Context<Group> context = createContext(responseHandler);
		groupServices.readGroup(context, groupId);
		if (!responseHandler.getRows().isEmpty()) {
			return responseHandler.getRow();
		}
		validateMessages(responseHandler.getMessages());
		return null;
	}
	
	
	
	private <T> DefaultResponseHandler<T> createResponseHandler(Class<T> t) {
		return new DefaultResponseHandler<T>(t); 
	}
	
	private <T> Context<T> createContext(DefaultResponseHandler<T> responseHandler) {
		return new ContextBuilder<T>()
				.createCommonRequestArgs(adminUser, "", "", "", hostname).setResponseHandler(responseHandler).build();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		Preconditions.checkNotNull(hostname,"Hostname is mandatory (used in ContextRequestArgs, domain)");
		Preconditions.checkNotNull(adminUser,"adminUser is mandatory (used for user, group creation) ");
		Preconditions.checkNotNull(userServices, "User services is mandatory");
		Preconditions.checkNotNull(firmServices, "Firm services is mandatory");
		Preconditions.checkNotNull(groupServices, "Group services is mandatory");
	}


	public void setAdminUser(String adminUser) {
		this.adminUser = adminUser;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public void setUserServices(UserServices userServices) {
		this.userServices = userServices;
	}

	public void setFirmServices(FirmServices firmServices) {
		this.firmServices = firmServices;
	}

	public void setGroupServices(GroupServices groupServices) {
		this.groupServices = groupServices;
	}

}
