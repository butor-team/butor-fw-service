/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.quartz;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.SchedulerRepository;
import org.quartz.impl.StdSchedulerFactory;

import com.google.common.base.Strings;

/**
 * @author asawan
 *
 */
public class QuartzJob {
	public QuartzJob(String name, Job job, String cronExpression) throws SchedulerException {
		this(name, null, job, cronExpression);
	}
	public QuartzJob(String name, String group, Job job, String cronExpression) throws SchedulerException {
		JobDataMap jdm = new JobDataMap();
		jdm.put("delegate", job);
		
		JobBuilder jb = JobBuilder.newJob()
				.ofType(QuartzJobWrapper.class)
				.withDescription(name)
				.usingJobData(jdm)
				.storeDurably(true);
		
		TriggerBuilder<CronTrigger> tb = TriggerBuilder.newTrigger()
				.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression));

		if (Strings.isNullOrEmpty(group)) {
			jb.withIdentity(name);
			tb.withIdentity(name);
		} else {
			jb.withIdentity(name, group);
			tb.withIdentity(name, group);
		}

		Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

		SchedulerRepository sr = SchedulerRepository.getInstance();
		try {
			sr.bind(scheduler);
		} catch (SchedulerException e) {
			// ok already binded
		}

		scheduler.scheduleJob(jb.build(), tb.build());
	}
}
