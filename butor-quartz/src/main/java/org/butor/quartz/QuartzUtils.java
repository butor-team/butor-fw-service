/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.quartz;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.butor.utils.ApplicationException;
import org.butor.utils.CommonDateFormat;
import org.butor.utils.CommonMessageID;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.impl.SchedulerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class QuartzUtils {
	final private static Logger logger = LoggerFactory.getLogger(QuartzUtils.class);
	
	/** 
	 * This method is invoked while a job is running to know if it is the last run of the day
	 * If the next scheduled run is not in the same yyyy-mm-dd then we consider the current
	 * run is the last one.
	 * @param jobName
	 * @return true if the current run is last of the day
	 * @throws Exception
	 */
	public static boolean isDayLastRun(String jobName) throws Exception {
		String today = CommonDateFormat.YYYYMMDD.format(new Date());
		Date nextRunTime = getNextRunTime(jobName);
		return nextRunTime != null && !CommonDateFormat.YYYYMMDD.format(nextRunTime).equals(today);
	}
	public static Date getNextRunTime(String jobName) throws Exception {
		SchedulerRepository sr = SchedulerRepository.getInstance();
		Collection<Scheduler> sc = sr.lookupAll();
		if (sc == null) {
			ApplicationException.exception(CommonMessageID.NOT_FOUND.getMessage("Scheduler"));
		}
		List<? extends Trigger> tl = null;
		JobKey jk = new JobKey(jobName);
		
		for (Scheduler s : sc) {
			tl = s.getTriggersOfJob(jk);
			if (tl != null && tl.size() > 0) {
				return tl.get(0).getNextFireTime();
			}

			List<String> gnl = s.getJobGroupNames();
			for (String gn : gnl) {
				jk = new JobKey(jobName, gn);
				tl = s.getTriggersOfJob(jk);
				if (tl != null && tl.size() > 0) {
					return tl.get(0).getNextFireTime();
				}
			}
		}
		
		logger.warn("Did not found schedule of quartz job: " +jobName);
		return null;
	}
}
