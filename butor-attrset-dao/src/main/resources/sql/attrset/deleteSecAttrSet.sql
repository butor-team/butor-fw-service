--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

DELETE at.*
FROM attrSet at join (__authDataSql__ ) au ON
	(au.d1 = '*' or at.type = au.d1) AND
	(au.d2 = '*' or at.id = au.d2) AND
	(au.d3 = '*' or at.k1 = au.d3) AND
	(au.d4 = '*' or at.k2 = au.d4) 
WHERE
	type = :type AND
	id = :id AND
	k1 = :k1 AND
	k2 = :k2
