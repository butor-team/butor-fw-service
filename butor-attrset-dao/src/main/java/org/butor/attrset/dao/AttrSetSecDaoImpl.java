/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.attrset.dao;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.butor.attrset.common.AttrSet;
import org.butor.attrset.common.AttrSetCriteria;
import org.butor.auth.common.AuthDataFilter;
import org.butor.dao.AbstractDao;
import org.butor.dao.DAOMessageID;
import org.butor.json.CommonRequestArgs;
import org.butor.utils.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AttrSetSecDaoImpl extends AbstractDao implements AttrSetDao {

	protected static Logger logger = LoggerFactory.getLogger(AttrSetSecDaoImpl.class);

	private final String PROC_GET_ATTR_SET = getClass().getName() +".getAttrSet";
	private final String PROC_GET_AND_LOCK_ATTR_SET = getClass().getName() +".getAndLockAttrSet";
	private final String PROC_READ_ATTR_SET = getClass().getName() +".readAttrSet";
	private final String PROC_INSERT_ATTR_SET = getClass().getName() +".insertAttrSet";
	private final String PROC_UPDATE_ATTR_SET = getClass().getName() +".updateAttrSet";
	private final String PROC_DELETE_ATTR_SET = getClass().getName() +".deleteAttrSet";
	
	private final String SYSTEM = "sec";
	private final String DATATYPES = "attrSet";
	private final String FUNCTION = "attrSet";	

	private String listSQL;
	private String listAndLockSQL;
	private String readSQL;
	private String insertSQL;
	private String updateSQL;
	private String deleteSQL;
	private String tableName = "attrSet";
	
	private AuthDataFilter buildAuthDataFilter(CommonRequestArgs cra) {
		AuthDataFilter authDataFilter = new AuthDataFilter();		
		authDataFilter.setSys(SYSTEM).setDataTypes(DATATYPES).setFunc(FUNCTION).setMember(cra.getUserId());
		return authDataFilter;
	}

	@Override
	public List<AttrSet> getAttrSet(AttrSetCriteria criteria, CommonRequestArgs cra) {
		AuthDataFilter authDataFilter = buildAuthDataFilter(cra);
		return queryList(PROC_GET_ATTR_SET, listSQL, AttrSet.class, criteria, authDataFilter,cra);
		       
	}
	
	@Override
	public List<AttrSet> getAndLockAttrSet(AttrSetCriteria criteria, CommonRequestArgs cra) {
		AuthDataFilter authDataFilter = buildAuthDataFilter(cra);
		List<AttrSet> list = queryList(PROC_GET_AND_LOCK_ATTR_SET, listAndLockSQL, AttrSet.class, criteria, authDataFilter,cra);
		// We do the sort in Java, not in the SQL
		// We do that because adding an "order by" in the SQL would cause more rows to be locked in innodb (believe it or not)
		// This leads to undesirable effects, like DB deadlocks where no one would expect
		Collections.sort(list, new Comparator<AttrSet>() {
			@Override
			public int compare(AttrSet o1, AttrSet o2) {
				return o1.getSeq() < o2.getSeq() ? -1 : (o1.getSeq() == o2.getSeq() ? 0 : 1);
			}
		});
		return list;
	}

	@Override
	public AttrSet readAttrSet(AttrSetCriteria criteria, CommonRequestArgs cra) {
		AuthDataFilter authDataFilter = buildAuthDataFilter(cra);
		return query(PROC_READ_ATTR_SET, readSQL, AttrSet.class,criteria,authDataFilter,cra);
	}
	
	@Override
	public AttrSet insertAttrSet(AttrSet attrSet, CommonRequestArgs cra) {
		AuthDataFilter authDataFilter = buildAuthDataFilter(cra);
		UpdateResult ur = update(PROC_INSERT_ATTR_SET, insertSQL, attrSet,authDataFilter,cra);		
		if (ur.numberOfRowAffected <= 0) {
			throw ApplicationException.exception(DAOMessageID.INSERT_FAILURE.getMessage()); 
		}
		return readAttrSet(AttrSetCriteria.valueOf(attrSet.getType(),attrSet.getId(),attrSet.getK1(),attrSet.getK2()),cra);
	}
	
	@Override
	public AttrSet updateAttrSet(AttrSet attrSet, CommonRequestArgs cra) {
		AuthDataFilter authDataFilter = buildAuthDataFilter(cra);
		UpdateResult ur = update(PROC_UPDATE_ATTR_SET, updateSQL, attrSet,authDataFilter,cra);
		if (ur.numberOfRowAffected == 0) {
			throw ApplicationException.exception(DAOMessageID.UPDATE_FAILURE.getMessage()); 
		} 
		return readAttrSet(AttrSetCriteria.valueOf(attrSet.getType(),attrSet.getId(),attrSet.getK1(),attrSet.getK2()),cra);
	}

	protected String checkTableName(String sql) {
		if (sql != null) {
			sql = sql.replace("attrSet", tableName);
		}
		return sql;
	}
	@Override
	public void deleteAttrSet(AttrSetCriteria criteria, CommonRequestArgs cra) {
		AuthDataFilter authDataFilter = buildAuthDataFilter(cra);
		UpdateResult ur = update(PROC_DELETE_ATTR_SET, deleteSQL, criteria,authDataFilter,cra);
		if (ur.numberOfRowAffected == 0) {
			ApplicationException.exception(DAOMessageID.DELETE_FAILURE.getMessage()); 
		}
	}
	
	public void setListSQL(String listSql) {
		this.listSQL = checkTableName(listSql);
	}
	public void setListAndLockSQL(String listAndLockSQL) {
		this.listAndLockSQL = checkTableName(listAndLockSQL);
	}
	public void setReadSQL(String readSQL) {
		this.readSQL = checkTableName(readSQL);
	}
	public void setInsertSQL(String insertSQL) {
		this.insertSQL = checkTableName(insertSQL);
	}
	public void setUpdateSQL(String updateSQL) {
		this.updateSQL = checkTableName(updateSQL);
	}
	public void setDeleteSQL(String deleteSQL) {
		this.deleteSQL = checkTableName(deleteSQL);
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
		listSQL = checkTableName(listSQL);
		listAndLockSQL = checkTableName(listAndLockSQL);
		readSQL = checkTableName(readSQL);
		insertSQL = checkTableName(insertSQL);
		updateSQL = checkTableName(updateSQL);
		deleteSQL = checkTableName(deleteSQL);
	}
}