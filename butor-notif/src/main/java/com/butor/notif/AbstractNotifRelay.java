/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.notif;

import org.butor.json.JsonHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * 
 * @author asawan
 * 
 */
public abstract class AbstractNotifRelay implements NotifProducer {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private JsonHelper jsh = new JsonHelper();
	private EventBus eventBus;
	
	public AbstractNotifRelay(EventBus eventBus) {
		this.eventBus = Preconditions.checkNotNull(eventBus);
		eventBus.register(this);
	}
	
	/**
	 * process outbound message.
	 * this message come from internal process headed to outside through
	 * the relay
	 * @param event OutboundNotif
	 */
	@Subscribe
	public void processOutboundMessage(OutboundNotif event) {
		if (event == null) {
			return;
		}
		try {
			if (event.getData().containsKey("__passed__")) {
				// ignore this. already passed by
				logger.warn("Dropping already __passed__ message! " +event);
				return;
			}
			event.getData().put("__passed__", this.getClass().getName());
			final String notif = jsh.serialize(event);
			postMessage(notif);
		} catch (Exception e) {
			logger.warn("failed while processing event! " +event);
		}
	}
	
	/**
	 * process incoming from outside (from a topic for exemple) message
	 * 
	 * @param serializedServerNotif String
	 */
	public void processInboundMessage(String serializedServerNotif) {
		try {
			InboundNotif notif = jsh.deserialize(serializedServerNotif, InboundNotif.class);
			if (notif == null) {
				logger.warn("Failed to deserialize notif! {}", serializedServerNotif);
				return;
			}
			eventBus.post(notif);
		} catch (Exception e) {
			logger.warn("failed while processing jms message!");
		}
	}
}
