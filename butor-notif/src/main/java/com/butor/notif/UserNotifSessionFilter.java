/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.notif;

import com.google.api.client.util.Strings;

/**
 * allow notification to be sent to client only if
 * the notification message is specifically sent to that client.
 * 
 * the session owner (session.username) match the "username" data attribute of 
 * the notification message
 * 
 * @author asawan
 */
public class UserNotifSessionFilter implements NotifSessionFilter {
	/**
	 * Check if a Notification session (a client) is allowed to receive
	 * a notification message
	 * 
	 * @param notif Notification
	 * @param session NotifSession
	 * @return true if the session is allowed to receive the notification.
	 */
	@Override
	public boolean accept(Notification notif, NotifSession session) {
		return !Strings.isNullOrEmpty(notif.getTo()) && notif.getTo().equals(session.getUsername());
	}
}
