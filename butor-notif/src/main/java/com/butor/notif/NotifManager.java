/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.notif;




/**
 * @author asawan
 *
 */
public interface NotifManager {
	/**
	 * shut down the manager
	 */
	void shutdown();
	/**
	 * add a new client notification session
	 * @param ns NotifSession
	 */
	void addSession(NotifSession ns);
	/**
	 * remove a client notification session
	 * @param sessionId String
	 */
	void removeSession(String sessionId);
	/**
	 * send a message to applicable client sessions
	 * @param notif Notification
	 */
	void post(Notification notif);
	/**
	 * receive/handle a message from a client session
	 * @param notif OutboundNotif
	 */
	void handleClientNotif(OutboundNotif notif);
}
