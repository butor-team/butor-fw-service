/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.butor.portal.common.userProfile;

import java.util.List;

import org.butor.attrset.common.AttrSet;
import org.butor.json.CommonRequestArgs;

public interface UserProfileModel {
	List<AttrSet> readProfile(CommonRequestArgs cra);
	AttrSet updateAttr(AttrSet attr, CommonRequestArgs cra);
	void deleteAttr(AttrSet attr, CommonRequestArgs cra);
}
