/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.transformer;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.butor.json.JsonHelper;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;

/**
 * <code>StringToObjectArray</code> converts a String into an object array. This
 * is useful in certain situations, as when a string needs to be converted into
 * an Object[] in order to be passed to a SOAP service. The input String is parsed
 * into the array based on a configurable delimiter - default is a space.
 */
public class Json2Object extends AbstractTransformer
{
    private Class javaClass = null;

    public Class getJavaClass() {
		return javaClass;
	}

	public void setJavaClass(Class javaClass) {
		this.javaClass = javaClass;
	}

	private JsonHelper _jsh = new JsonHelper();
    
    @Override
    public Object doTransform(Object src, String outputEncoding) throws TransformerException
    {
        String in;

        if (src instanceof String)
        {

        	
			Object obj = _jsh.deserialize((String)src, javaClass);
/*
			try {
				//obj = _jsh.deserialize(URLDecoder.decode((String)src, "UTF-8").substring(1), javaClass);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
*/
			return obj;
        }
        return null;
    }

}
