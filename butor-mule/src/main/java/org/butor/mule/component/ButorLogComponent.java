/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.component;

import org.mule.api.MuleEventContext;
import org.mule.api.component.simple.LogService;
import org.mule.api.lifecycle.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ButorLogComponent implements LogService, Callable {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	private int maxPayloadLengthToLog = -1;

	public void setMaxPayloadLengthToLog(int maxPayloadLengthToLog) {
		this.maxPayloadLengthToLog = maxPayloadLengthToLog;
	}

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		String payload = eventContext.getMessageAsString();
		if (maxPayloadLengthToLog < 0 || payload.length() <= maxPayloadLengthToLog) {
			log(String.format("Payload: %s", payload));
		} else {
			log(String.format("Payload: %s chars (full payload in debug)", String.valueOf(payload.length())));
			logger.debug(String.format("Payload: %s", payload));
		}
		return eventContext.getMessage();
	}

	@Override
	public void log(String message) {
		logger.info(message);
	}

}
