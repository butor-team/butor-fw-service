/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.component;

import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.butor.json.JsonServiceRequest;
import org.butor.json.service.Context;
import org.butor.json.service.ServiceManager;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public abstract class ButorServiceExecutorComponent implements Callable {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	private ServiceManager serviceManager;
	private ExecutorService executor;
	private int maxPayloadLengthToLog = -1;
	private Set<String> servicesToNotLogArgs = Collections.emptySet();
	protected Set<String> servicesToNotLogResponses = Collections.emptySet();

	@Override
	public Object onCall(MuleEventContext context) throws Exception {
		final MuleMessage mmsg = context.getMessage();
		if (!(mmsg.getPayload() instanceof JsonServiceRequest))
			return mmsg;
		


		final JsonServiceRequest req = (JsonServiceRequest) mmsg.getPayload();

		final String logReqInfo = String.format("ns: %s, service: %s, reqId: %s, sessionId: %s, userId: %s, domain: %s",  
				req.getNamespace(), req.getService(), req.getReqId(), req.getSessionId(), req.getUserId(), req.getDomain());

		boolean logArgs = !this.servicesToNotLogArgs.contains(req.getNamespace() +";" +req.getService());
		if (logArgs) {
			if (maxPayloadLengthToLog < 0 || req.getServiceArgsJson().length() <= maxPayloadLengthToLog) {
				logger.info(String.format("REQUEST: %s, args: %s",	
					logReqInfo, req.getServiceArgsJson()));
			} else {
				if (logger.isDebugEnabled()) {
					logger.debug(String.format("REQUEST: %s, args: %s",	
						logReqInfo, req.getServiceArgsJson()));
				} else {
					int argsLen = req.getServiceArgsJson().length();
					String chunck = req.getServiceArgsJson().substring(0, maxPayloadLengthToLog);
					logger.info(String.format("REQUEST: %s, args: %s... %d chars (truncated - full args in debug level)",
						logReqInfo, chunck, argsLen));
				}
			}
		} else {
			logger.info(String.format("REQUEST: %s, args: %s",	
				logReqInfo, "/*censored*/"));
		}

		final PipedOutputStream pos = new PipedOutputStream();
		final PipedInputStream pis = new PipedInputStream(pos);

		Runnable worker = null;
		if (serviceManager.isBinary(req)) {
			if(mmsg.getInboundAttachmentNames() != null && !mmsg.getInboundAttachmentNames().isEmpty()){
				String inboundAttachementNames = mmsg.getInboundAttachmentNames().toArray(new String[0])[0];				
				worker = createBinWorker(mmsg, mmsg.getInboundAttachment(inboundAttachementNames).getInputStream(), pos, req, logReqInfo);
			}
			else{
				worker = createBinWorker(mmsg, null, pos, req, logReqInfo);
			}
		} else {
			worker = createJsonWorker(mmsg, pos, req, logReqInfo);
		}
		executor.execute(worker);

		return pis;
	}

	public abstract Runnable createJsonWorker(MuleMessage mmsg, PipedOutputStream pos, 
			JsonServiceRequest req, String logReqInfo);

	public abstract Runnable createBinWorker(MuleMessage mmsg, InputStream is, PipedOutputStream pos,
			JsonServiceRequest req, String logReqInfo);

	public void invoke(Context ctx) {
		serviceManager.invoke(ctx);
	}
	public ExecutorService getExecutor() {
		return executor;
	}

	public void setExecutor(ExecutorService executor) {
		this.executor = executor;
	}

	public ServiceManager getServiceManager() {
		return this.serviceManager;
	}

	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	}

	public void setMaxPayloadLengthToLog(int maxPayloadLengthToLog) {
		this.maxPayloadLengthToLog = maxPayloadLengthToLog;
	}

	public void setServicesToNotLogArgs(Set<String> servicesToNotLogArgs) {
		this.servicesToNotLogArgs = servicesToNotLogArgs;
		if (this.servicesToNotLogArgs == null) {
			this.servicesToNotLogArgs = Collections.emptySet();
		}
	}

	public void setServicesToNotLogResponses(Set<String> servicesToNotLogResponses) {
		this.servicesToNotLogResponses = servicesToNotLogResponses;
		if (this.servicesToNotLogResponses == null) {
			this.servicesToNotLogResponses = Collections.emptySet();
		}
	}
}