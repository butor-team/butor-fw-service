/**
 * Copyright 2013-2019 Butor Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.mule.component;

import java.io.IOException;
import java.io.PipedOutputStream;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import org.butor.json.CommonRequestArgs;
import org.butor.json.JsonHelper;
import org.butor.json.JsonServiceRequest;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandler;
import org.butor.utils.Message;
import org.mule.api.MuleMessage;

import com.google.api.client.util.Lists;

/**
 * This implementation do not stream json response
 * it will return a plain whole json payload
 */
public class ButorBufferedJsonServiceExecutorComponent extends ButorJsonServiceExecutorComponent {
	private JsonHelper jsh = new JsonHelper();

	@Override
	public Runnable createJsonWorker(final MuleMessage mmsg,
			final PipedOutputStream pos, final JsonServiceRequest req,
			final String logReqInfo) {
		
		final boolean logResponse = !this.servicesToNotLogResponses
				.contains(req.getNamespace() + ";" + req.getService());
		
		final List<Message> messages = Lists.newArrayList();
		
		mmsg.setInvocationProperty("Content-Type", "text/json");

		final ResponseHandler<Object> streamer = new ResponseHandler<Object>() {
			@Override
			public void end() {
				// ok
			}

			@Override
			public boolean addRow(Object row_) {
				if (row_ == null) {
					return false;
				}
				try {
					String chunk = jsh.serialize(row_);
					if (logResponse && logger.isDebugEnabled()) {
						logger.debug("RESPONSE: {}, row: {}", logReqInfo, chunk);
					}
					pos.write(chunk.getBytes());
					pos.flush();
					return true;
				} catch (IOException e) {
					logger.warn("Failed while writing a response row!", e);
				}
				return false;
			}

			@Override
			public boolean addMessage(Message message_) {
				messages.add(message_);
				if (logResponse && logger.isDebugEnabled()) {
					logger.debug("RESPONSE: {}, message: {}", logReqInfo,
							jsh.serialize(message_));
				}
				return true;
			}

			@Override
			public Type getResponseType() {
				return Object.class;
			}
		};
		final Context ctx = new Context() {
			@Override
			public ResponseHandler<Object> getResponseHandler() {
				return streamer;
			}

			@Override
			public CommonRequestArgs getRequest() {
				return req;
			}
		};

		Runnable worker = new Runnable() {
			@Override
			public void run() {
				long time = System.currentTimeMillis();
				boolean success = false;
				Date serviceCallTimestamp = new Date();
				try {
					pos.write(String.format("{\\\"reqId\\\":\\\"%s\\\", \"data\":[", req.getReqId()).getBytes());
					invoke(ctx);
					pos.write("], \"messages\":".getBytes());
					pos.write(jsh.serialize(messages).getBytes());
					pos.write("}".getBytes());
					pos.write(0);// null Delimiter
					pos.flush();
					pos.close();
					success = true;
				} catch (Exception e) {
					logger.warn("Failed while invoking service!", e);
				} finally {
					long elapsed = System.currentTimeMillis() - time;
					Object[] args = new Object[] { logReqInfo,
							Boolean.valueOf(success), Long.valueOf(elapsed) };
					logger.info("STATS: {}, success: {}, elapsed: {} ms", args);
					
					// submit (use thread pool for JDBC insertion)
					jdbcLogging(req, ctx, success, serviceCallTimestamp, elapsed);
				}
			}
		};

		return worker;
	}

}