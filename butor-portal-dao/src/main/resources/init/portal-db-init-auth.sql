--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--


# GROUPS
DELETE FROM  `secDesc` where idType='group';
INSERT INTO `secDesc` (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','group', 'Security admin group',NOW(),'load',0)
;
DELETE FROM  `secGroup`;
INSERT INTO `secGroup` (`groupId`,`member`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','admin@this.portal',NOW(),'load',0)
;

# ROLES
DELETE FROM  `secDesc` where idType='role';
INSERT INTO `secDesc` (`id`,`idType`, `description`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','role', 'Security admin role',NOW(),'load',0)
;
DELETE FROM  `secRole`;
INSERT INTO `secRole` (`roleId`,`func`,`sys`,`mode`,`stamp`,`userId`,`revNo`) VALUES
	('sec.admin','sec','sec',2,NOW(),'load',0),
	('sec.admin','firms','sec',2,NOW(),'load',0),
	('sec.admin','users','sec',2,NOW(),'load',0),
	('sec.admin','groups','sec',2,NOW(),'load',0),
	('sec.admin','funcs','sec',2,NOW(),'load',0),
	('sec.admin','roles','sec',2,NOW(),'load',0),
	('sec.admin','auths','sec',2,NOW(),'load',0)
;

# DATA
DELETE FROM  `secData`;
INSERT INTO `secData` (`dataId`,`sys`,`dataType`,`d1`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec','firm','*',NOW(),'load',0),
	(1,'sec','user','*',NOW(),'load',0),
	(1,'sec','group','*',NOW(),'load',0),
	(1,'sec','role','*',NOW(),'load',0),
	(1,'sec','func','*',NOW(),'load',0),
	(2,'sec','firm','*',NOW(),'load',0),
	(3,'sec','user','*',NOW(),'load',0);

# authorizations
# mode = [0-2]
# 0: nothing, 
# 1: read, 
# 2: (1) +write 
DELETE FROM  `secAuth`;
INSERT INTO `secAuth` (`authId`,`who`,`whoType`,`what`,`whatType`,`sys`,`dataId`,`mode`,`startDate`,`endDate`,`stamp`,`userId`,`revNo`) VALUES
	(1,'sec.admin','group','sec.admin','role','',1,0,null,null,NOW(),'load',0),
	(2,'portal','user','firms','func','sec',2,2,null,null,NOW(),'load',0),
	(3,'portal','user','users','func','sec',3,2,null,null,NOW(),'load',0),
	(4,'admin@this.portal','user','support','func','portal',-2,2,null,null,NOW(),'load',0)
;
