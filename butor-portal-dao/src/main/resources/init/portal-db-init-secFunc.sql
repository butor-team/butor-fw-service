--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

# security functions ---------------
DELETE FROM  `secFunc` where sys='sec' or sys='portal';
INSERT INTO `secFunc` (`func`, `sys`, `description`, `stamp`, `userId`,`revNo`) VALUES 
	('portal', 'portal', 'Portal', NOW(), 'load',0),
	('support', 'portal', 'Portal Support', NOW(), 'load',0),
	('sec','sec','Security', NOW(), 'load',0),
	('firms','sec','Security firms', NOW(), 'load',0),
	('users','sec','Security users', NOW(), 'load',0),
	('funcs','sec','Security functions', NOW(), 'load',0),
	('roles','sec','Security roles', NOW(), 'load',0),
	('groups','sec','Security groups', NOW(), 'load',0),
	('auths','sec','Security authorisations', NOW(), 'load',0),
	('audit','sec','Security user login audit', NOW(), 'load',0),
	('auditServiceCall','sec','Security service call audit', NOW(), 'load',0),
	('apiKey','sec','Security API Key', NOW(), 'load',0)
;

