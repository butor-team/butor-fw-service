--
-- Copyright 2013-2019 Butor Inc.
--
-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
--
--   http://www.apache.org/licenses/LICENSE-2.0
--
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

# attrSet -------------------------------
# systems
DELETE FROM  `attrSet` where id = 'systems';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES
	('codeset','systems','portal', 1, 'fr', 'Portail', NOW(), 'load',0),
	('codeset','systems','portal', 1, 'en', 'Portal', NOW(), 'load',0),
	('codeset','systems','sec', 3, 'fr', 'Sécurité', NOW(), 'load',0),
	('codeset','systems','sec', 3, 'en', 'Security', NOW(), 'load',0),
	('codeset','systems','sso', 5, 'fr', 'SSO', NOW(), 'load',0),
	('codeset','systems','sso', 5, 'en', 'SSO', NOW(), 'load',0)
	;

# theme
DELETE FROM  `attrSet` where id = 'theme';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','theme','BUB8073A55', 2, '', 'Butor.com', NOW(), 'load',0)
	;

# Sign In pre-defined questions
DELETE FROM  `attrSet` where id = 'sign-q';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','sign-q','1', 1, 'fr', "Le nom de votre animal de compagnie?", NOW(), 'load',0),
	('codeset','sign-q','1', 1, 'en', "Your pet's name?", NOW(), 'load',0),
	('codeset','sign-q','2', 2, 'fr', "Nom de jeune fille de votre mère?", NOW(), 'load',0),
	('codeset','sign-q','2', 2, 'en', "Your mother's maiden name?", NOW(), 'load',0),
	('codeset','sign-q','3', 3, 'fr', "Nom de l'école secondaire que vous avez frequenté?", NOW(), 'load',0),
	('codeset','sign-q','3', 3, 'en', "Name of high school you have attended?", NOW(), 'load',0),
	('codeset','sign-q','4', 4, 'fr', "Nom de l'école primaire que vous avez frequenté?", NOW(), 'load',0),
	('codeset','sign-q','4', 4, 'en', "Name of primary school you have attended?", NOW(), 'load',0),
	('codeset','sign-q','5', 5, 'fr', "Nom de la rue où vous avez vecu votre enfance?", NOW(), 'load',0),
	('codeset','sign-q','5', 5, 'en', "Street name where you lived your childhood?", NOW(), 'load',0),
	('codeset','sign-q','6', 6, 'fr', "Prénom de votre enfant le plus âgé?", NOW(), 'load',0),
	('codeset','sign-q','6', 6, 'en', "Middle name of your oldest child?", NOW(), 'load',0),
	('codeset','sign-q','7', 7, 'fr', "L'anniversaire de votre frère aîné (mois/année)?", NOW(), 'load',0),
	('codeset','sign-q','7', 7, 'en', "Your oldest brother's birthday (month/year)?", NOW(), 'load',0),
	('codeset','sign-q','10', 10, 'fr', 'Autre', NOW(), 'load',0),
	('codeset','sign-q','10', 10, 'en', 'Other', NOW(), 'load',0);

# Sign In pre-defined avatars
DELETE FROM  `attrSet` where id = 'sign-avatar';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','sign-avatar','fish', 1, '', 'fish.jpg', NOW(), 'load',0),
	('codeset','sign-avatar','tire', 2, '', 'tire.jpg', NOW(), 'load',0),
	('codeset','sign-avatar','rose1', 3, '', 'rose1.jpg', NOW(), 'load',0),
	('codeset','sign-avatar','rose2', 4, '', 'rose2.jpg', NOW(), 'load',0),
	('codeset','sign-avatar','sunflower', 5, '', 'sunflower.jpg', NOW(), 'load',0),
	('codeset','sign-avatar','android', 6, '', 'android.jpg', NOW(), 'load',0),
	('codeset','sign-avatar','dandelion', 7, '', 'dandelion.jpg', NOW(), 'load',0),
	('codeset','sign-avatar','apple', 8, '', 'apple.jpg', NOW(), 'load',0);

# authorisation who types
DELETE FROM  `attrSet` where id = 'auth-who-type';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','auth-who-type','user', 1, 'fr', 'Usager', NOW(), 'load',0),
	('codeset','auth-who-type','user', 1, 'en', 'User', NOW(), 'load',0),
	('codeset','auth-who-type','group', 2, 'fr', "Groupe d'usager", NOW(), 'load',0),
	('codeset','auth-who-type','group', 2, 'en', 'User group', NOW(), 'load',0);

# authorisation what types
DELETE FROM  `attrSet` where id = 'auth-what-type';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','auth-what-type','func', 1, 'fr', 'Fonction', NOW(), 'load',0),
	('codeset','auth-what-type','func', 1, 'en', 'Function', NOW(), 'load',0),
	('codeset','auth-what-type','role', 2, 'fr', 'Rôle', NOW(), 'load',0),
	('codeset','auth-what-type','role', 2, 'en', 'Role', NOW(), 'load',0);

# authorisation mode
# mode = [0-2]
# 0: nothing, 
# 1: read, 
# 2: (1) +write 
DELETE FROM  `attrSet` where id = 'auth-mode';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','auth-mode','1', 1, 'fr', 'Lecture', NOW(), 'load',0),
	('codeset','auth-mode','1', 1, 'en', 'Read', NOW(), 'load',0),
	('codeset','auth-mode','2', 2, 'fr', 'Lecture, Écriture', NOW(), 'load',0),
	('codeset','auth-mode','2', 2, 'en', 'Read, Write', NOW(), 'load',0);


# lang
DELETE FROM  `attrSet` where id = 'lang';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','lang','en', 1, 'en', 'English', NOW(), 'load',0),
	('codeset','lang','en', 1, 'fr', 'Anglais', NOW(), 'load',0),
	('codeset','lang','fr', 2, 'en', 'French', NOW(), 'load',0),
	('codeset','lang','fr', 2, 'fr', 'Français', NOW(), 'load',0);

# lang-switch
DELETE FROM  `attrSet` where id = 'lang-switch';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('codeset','lang-switch','en', 1, 'en', 'English', NOW(), 'load',0),
	('codeset','lang-switch','en', 1, 'fr', 'English', NOW(), 'load',0),
	('codeset','lang-switch','fr', 2, 'en', 'Français', NOW(), 'load',0),
	('codeset','lang-switch','fr', 2, 'fr', 'Français', NOW(), 'load',0);

# email templates
DELETE FROM  `attrSet` where id = 'email-template';
INSERT INTO `attrSet` (`type`, `id`, `k1`, `seq`, `k2`, `value`, `stamp`, `userId`,`revNo`) VALUES 
	('reset-login','email-template','*', 1, 'en', '{"from":"", "subject":"Portal login reset", "message":"Hello {username},\n\nYour login to the portal has been reset as you requested.\nPlease click on the link bellow and follow the instructions.\n\n{link}\n\nFor assistance, please contact us at 514 999 7741\n\n--\nBest regards.\nSupport team\nhttp://www.butor.com"}', NOW(), 'load',0),
	('reset-login','email-template','*', 1, 'fr', '{"from":"", "subject":"Reinitialisation login portail", "message":"Bonjour {username},\n\nVotre login au portail a été reinitialisé à votre demande.\nSVP cliquer sur le lien plus bas et suivre les instructions.\n\n{link}\n\nPour assitance, SVP nous contactez au 514 999 7741\n\n--\nMeilleures salutations.\nÉquipe de support\nhttp://www.butor.com"}', NOW(), 'load',0)
;
